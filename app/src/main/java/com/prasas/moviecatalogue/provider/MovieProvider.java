package com.prasas.moviecatalogue.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.prasas.moviecatalogue.dao.MovieDao;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.data.repository.LocalRepository;
import com.prasas.moviecatalogue.database.AppDatabase;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class MovieProvider extends ContentProvider {

    private AppDatabase appDatabase;
    private LocalRepository localRepository;
    public static final String AUTHORITY = "com.prasas.moviecatalogue";
    private static final String SCHEME = "content";

    private static final int MOVIE = 1;
    private static final int MOVIE_ID = 2;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, Constant.TABLE_NAME_MOVIE, MOVIE);
        uriMatcher.addURI(AUTHORITY, Constant.TABLE_NAME_MOVIE + "/#", MOVIE_ID);
    }

    @Override
    public boolean onCreate() {
        appDatabase = AppDatabase.getInstance(getContext());
        MovieDao movieDao = appDatabase.getMovieDao();
        localRepository = LocalRepository.getInstance(movieDao, appDatabase);

        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] strings, String s, String[] strings1, String s1) {
        if (uri.getPathSegments().get(0).equals(Constant.TABLE_NAME_MOVIE)){
            List<Movie> movieList = localRepository.getAllFavoriteMovie();

            String[] menuCols = new String[] {
                    Constant.COLUMN_ID, Constant.COLUMN_TITLE, Constant.COLUMN_VOTE,
                    Constant.COLUMN_VOTE_AVERAGE, Constant.COLUMN_POSTER_PATH, Constant.COLUMN_BACKDROP_PATH, Constant.COLUMN_GENRE, Constant.COLUMN_ADULT,
                    Constant.COLUMN_OVERVIEW, Constant.COLUMN_RELEASE_DATE, Constant.COLUMN_FAVORITE
            };
            MatrixCursor cursor = new MatrixCursor(menuCols);

            for (Object m : movieList) {
                Movie movie = (com.prasas.moviecatalogue.data.entity.Movie)m;
                String genreString = Util.genreListToString(movie.getGenre_ids());

                cursor.addRow(new Object[]{
                        movie.getId(), movie.getTitle(), movie.getVoteCount(), movie.getVoteAverage(), movie.getPosterPath(),
                        movie.getBackdrop_path(), genreString, (movie.isAdult() ? 1 : 0), movie.getOverview(),
                        movie.getReleaseDate(), (movie.isFavorite() ? 1 : 0)
                });
            }

            return cursor;
        }else {
            List<TvMovie> tvMovieList = localRepository.getAllFavoriteTvMovie();

            String[] menuCols = new String[] {
                    Constant.COLUMN_ID, Constant.COLUMN_TITLE, Constant.COLUMN_VOTE,
                    Constant.COLUMN_VOTE_AVERAGE, Constant.COLUMN_POSTER_PATH, Constant.COLUMN_BACKDROP_PATH, Constant.COLUMN_GENRE, Constant.COLUMN_ADULT,
                    Constant.COLUMN_OVERVIEW, Constant.COLUMN_RELEASE_DATE, Constant.COLUMN_FAVORITE
            };
            MatrixCursor cursor = new MatrixCursor(menuCols);

            for (Object tvM : tvMovieList) {
                TvMovie tvMovie = (TvMovie) tvM;
                String genreString = Util.genreListToString(tvMovie.getGenre_ids());

                cursor.addRow(new Object[]{
                        tvMovie.getId(), tvMovie.getName(), tvMovie.getVoteCount(), tvMovie.getVoteAverage(), tvMovie.getPosterPath(),
                        tvMovie.getBackdrop_path(), genreString, (tvMovie.isAdult() ? 1 : 0), tvMovie.getOverview(),
                        tvMovie.getReleaseDate(), (tvMovie.isFavorite() ? 1 : 0)
                });
            }

            return cursor;
        }
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        if (uri.getPathSegments().get(0).equals(Constant.TABLE_NAME_MOVIE)){
            Movie movie = new Movie();
            movie.setId(contentValues.getAsInteger(Constant.COLUMN_ID));
            movie.setTitle(contentValues.getAsString(Constant.COLUMN_TITLE));
            movie.setPosterPath(contentValues.getAsString(Constant.COLUMN_POSTER_PATH));
            movie.setBackdrop_path(contentValues.getAsString(Constant.COLUMN_BACKDROP_PATH));
            ArrayList<Integer> genreList = Util.genreStringToList(contentValues.getAsString(Constant.COLUMN_GENRE));
            movie.setGenre_ids(genreList);
            movie.setVoteCount(contentValues.getAsInteger(Constant.COLUMN_VOTE));
            movie.setVoteAverage(contentValues.getAsFloat(Constant.COLUMN_VOTE_AVERAGE));
            movie.setAdult(contentValues.getAsInteger(Constant.COLUMN_ADULT) == 1);
            movie.setOverview(contentValues.getAsString(Constant.COLUMN_OVERVIEW));
            movie.setReleaseDate(contentValues.getAsString(Constant.COLUMN_RELEASE_DATE));
            movie.setFavorite(contentValues.getAsInteger(Constant.COLUMN_FAVORITE) == 1);

            localRepository.addFavoriteMovie(movie);
        }else {
            TvMovie tvMovie = new TvMovie();
            tvMovie.setId(contentValues.getAsInteger(Constant.COLUMN_ID));
            tvMovie.setTitle(contentValues.getAsString(Constant.COLUMN_TITLE));
            tvMovie.setPosterPath(contentValues.getAsString(Constant.COLUMN_POSTER_PATH));
            tvMovie.setBackdrop_path(contentValues.getAsString(Constant.COLUMN_BACKDROP_PATH));
            ArrayList<Integer> genreList = Util.genreStringToList(contentValues.getAsString(Constant.COLUMN_GENRE));
            tvMovie.setGenre_ids(genreList);
            tvMovie.setVoteCount(contentValues.getAsInteger(Constant.COLUMN_VOTE));
            tvMovie.setVoteAverage(contentValues.getAsFloat(Constant.COLUMN_VOTE_AVERAGE));
            tvMovie.setAdult(contentValues.getAsInteger(Constant.COLUMN_ADULT) == 1);
            tvMovie.setOverview(contentValues.getAsString(Constant.COLUMN_OVERVIEW));
            tvMovie.setReleaseDate(contentValues.getAsString(Constant.COLUMN_RELEASE_DATE));
            tvMovie.setFavorite(contentValues.getAsInteger(Constant.COLUMN_FAVORITE) == 1);

            localRepository.addFavoriteTvMovie(tvMovie);
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String s, String[] strings) {
        try {
            if (uri.getPathSegments().get(0).equals(Constant.TABLE_NAME_MOVIE)) {
                Movie movie = new Movie();
                movie.setId(Integer.valueOf(s));
                localRepository.removeFavoriteMovie(movie);
            }else {
                TvMovie tvMovie = new TvMovie();
                tvMovie.setId(Integer.valueOf(s));
                localRepository.removeFavoriteTvMovie(tvMovie);
            }
            return 1;
        }catch (Exception e){
            return 0;
        }
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
