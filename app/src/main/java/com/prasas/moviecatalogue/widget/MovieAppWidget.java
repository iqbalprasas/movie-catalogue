package com.prasas.moviecatalogue.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.ui.splash.SplashScreenActivity;
import com.prasas.moviecatalogue.utils.Constant;

/**
 * Implementation of App Widget functionality.
 */
public class MovieAppWidget extends AppWidgetProvider {

    public static final String TOAST_ACTION = "com.prasas.moviecatalogue.TOAST_ACTION";
    public static final String EXTRA_ITEM_TITLE = "com.prasas.moviecatalogue.EXTRA_ITEM_TITLE";
    public static final String EXTRA_ITEM_ID = "com.prasas.moviecatalogue.EXTRA_ITEM_ID";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Intent intent = new Intent(context, StackWidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        ComponentName componentName = new ComponentName(context, MovieAppWidget.class);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.favorite_movie_widget);
        remoteViews.setRemoteAdapter(R.id.stack_view, intent);
        remoteViews.setEmptyView(R.id.stack_view, R.id.empty_view);

        Intent toastIntent = new Intent(context, MovieAppWidget.class);
        toastIntent.setAction(MovieAppWidget.TOAST_ACTION);
        toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setPendingIntentTemplate(R.id.stack_view, toastPendingIntent);

        appWidgetManager.updateAppWidget(componentName, remoteViews);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.stack_view);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction() != null) {
            if (intent.getAction().equals(TOAST_ACTION)) {
                String title = intent.getStringExtra(EXTRA_ITEM_TITLE);
                int id = intent.getIntExtra(EXTRA_ITEM_ID, 0);
                Toast.makeText(context, title, Toast.LENGTH_SHORT).show();

                Intent intentMovie = new Intent(context, SplashScreenActivity.class);
                intentMovie.putExtra(Constant.OPEN_MOVIE_FROM_WIDGET, id);
                intentMovie.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intentMovie.setData(Uri.parse(intentMovie.toUri(Intent.URI_INTENT_SCHEME)));
                context.startActivity(intentMovie);
            }
        }
    }
}

