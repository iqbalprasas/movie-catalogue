package com.prasas.moviecatalogue.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.dao.MovieDao;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.repository.LocalRepository;
import com.prasas.moviecatalogue.database.AppDatabase;

import java.util.ArrayList;
import java.util.List;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private List<Movie> movieList = new ArrayList<>();
    private final Context mContext;
    private LocalRepository localRepository;

    StackRemoteViewsFactory(Context mContext) {
        this.mContext = mContext;
        AppDatabase appDatabase = AppDatabase.getInstance(mContext);
        MovieDao movieDao = appDatabase.getMovieDao();
        localRepository = LocalRepository.getInstance(movieDao, appDatabase);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        movieList = localRepository.getAllFavoriteMovie();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return movieList.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.item_widget);
        Movie movie = movieList.get(i);
        try {
            Bitmap bitmap = Glide.with(mContext)
                    .asBitmap()
                    .load(BuildConfig.PHOTO_BASE_URL_LOW + movie.getPosterPath())
                    .error(R.drawable.ic_image)
                    .submit()
                    .get();

            rv.setImageViewBitmap(R.id.imageView, bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle extras = new Bundle();
        extras.putString(MovieAppWidget.EXTRA_ITEM_TITLE, movie.getTitle());
        extras.putInt(MovieAppWidget.EXTRA_ITEM_ID, movie.getId());

        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
