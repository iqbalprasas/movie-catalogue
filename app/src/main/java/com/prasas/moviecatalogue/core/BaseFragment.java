package com.prasas.moviecatalogue.core;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFragment<M extends AndroidViewModel> extends Fragment implements BaseView {

    @BindView(R.id.progressbar_layout)
    FrameLayout progressBar;

    private boolean isClickEnabled = true;

    protected abstract Class<M> getViewModel();

    protected abstract void initViewModel(M viewModel);

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void onActivityCreated(Bundle instance, M viewModel);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            M viewModel = ViewModelProviders.of(getActivity()).get(getViewModel());
            onActivityCreated(savedInstanceState, viewModel);
        } else {
            Toast.makeText(getContext(), getString(R.string.error_message_general), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constant.STATE, Constant.STATE_ROTATED);
    }

    protected void setClickEnabled(boolean isClickEnabled) {
        this.isClickEnabled = isClickEnabled;
    }

    protected boolean isClickEnabled() {
        return isClickEnabled;
    }

    protected void showLoading(boolean isShow) {
        progressBar.setVisibility(isShow
                ? View.VISIBLE
                : View.GONE);
    }
}
