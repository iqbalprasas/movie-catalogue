package com.prasas.moviecatalogue.core;

import com.prasas.moviecatalogue.services.DisposableManager;

public abstract class BaseRepository {
    public void dispose(Object object){
        DisposableManager.getInstance().unregister(object);
    }
}
