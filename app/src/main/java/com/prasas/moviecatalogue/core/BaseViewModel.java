package com.prasas.moviecatalogue.core;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.prasas.moviecatalogue.data.repository.MovieRepository;

public abstract class BaseViewModel extends AndroidViewModel {

    public BaseViewModel(@NonNull Application application) {
        super(application);
    }

    protected abstract MovieRepository getMovieRepository();

    public void dispose(Object object){
        getMovieRepository().dispose(object);
    }
}
