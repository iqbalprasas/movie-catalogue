package com.prasas.moviecatalogue.core;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.PreferenceManager;
import com.prasas.moviecatalogue.utils.TinyDB;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.prasas.moviecatalogue.utils.Constant.IS_LANGUAGE_CHANGED;

public abstract class BaseActivity<M extends AndroidViewModel> extends AppCompatActivity implements BaseView {

    @BindView(R.id.toolbar_movie)
    Toolbar toolbar;

    protected TinyDB tinyDB;
    private Unbinder unbinder;
    private boolean isClickEnabled = true;

    protected abstract void onCreate(Bundle instance, M viewModel);

    protected abstract Class<M> getViewModel();

    protected abstract void initViewModel(M viewModel);

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract int getToolbarType();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        tinyDB = new TinyDB(this);
        tinyDB.putBoolean(IS_LANGUAGE_CHANGED, false);

        if ((getToolbarType() != Constant.TOOLBAR_TYPE_EMPTY)){
            unbinder = ButterKnife.bind(this);
            setSupportActionBar(toolbar);
        }

        if (getSupportActionBar() != null) {
            if (getToolbarType() != Constant.TOOLBAR_TYPE_COLLAPSING){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);

                toolbar.setNavigationIcon(R.drawable.ic_back);
                if (getToolbarType() == Constant.TOOLBAR_TYPE_DETAIL){
                    toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        }

        M viewModel = ViewModelProviders.of(this).get(getViewModel());
        onCreate(savedInstanceState, viewModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setClickEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if ((getToolbarType() != Constant.TOOLBAR_TYPE_COLLAPSING) && (getToolbarType() != Constant.TOOLBAR_TYPE_EMPTY)) {
            onBackPressed();
            return true;
        }else {
            return false;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(PreferenceManager.setLocale(newBase));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString(Constant.STATE, Constant.STATE_ROTATED);
    }

    @Override
    protected void onDestroy() {
       if (unbinder != null) unbinder.unbind();

       super.onDestroy();
    }

    protected void setClickEnabled(boolean isClickEnabled) {
        this.isClickEnabled = isClickEnabled;
    }

    protected boolean isClickEnabled(){
        return this.isClickEnabled;
    }
}
