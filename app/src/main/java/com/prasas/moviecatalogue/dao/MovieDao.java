package com.prasas.moviecatalogue.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;

import java.util.List;

import io.reactivex.Observable;

@Dao
public abstract class MovieDao {

    @Query("SELECT * FROM Movie")
    public abstract LiveData<List<Movie>> getAllMovie();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void addMovie(Movie movie);

    @Query("SELECT * FROM TvMovie")
    public abstract LiveData<List<TvMovie>> getAllTvMovie();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void addTvMovie(TvMovie tvMovie);

    @Delete
    public abstract void deleteMovie(Movie movie);

    @Delete
    public abstract void deleteTvMovie(TvMovie tvMovie);

    @Query("SELECT * FROM GenreByMovie")
    public abstract LiveData<List<GenreByMovie>> getAllGenreByMovie();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void addAllGenreByMovie(List<GenreByMovie> genreByMovieList);

    @Query("DELETE FROM GenreByMovie WHERE idMovie = :idMovie")
    public abstract void deleteGenreByMovie(int idMovie);

    @Transaction
    public void addMovieWithGenre(Movie movie, List<GenreByMovie> genreByMovieList) {
        addMovie(movie);
        addAllGenreByMovie(genreByMovieList);
    }

    @Transaction
    public void addTvMovieWithGenre(TvMovie tvMovie, List<GenreByMovie> genreByMovieList) {
        addTvMovie(tvMovie);
        addAllGenreByMovie(genreByMovieList);
    }

    @Transaction
    public void deleteMovieWithGenre(Movie movie) {
        deleteMovie(movie);
        deleteGenreByMovie(movie.getId());
    }

    @Transaction
    public void deleteTvMovieWithGenre(TvMovie tvMovie) {
        deleteTvMovie(tvMovie);
        deleteGenreByMovie(tvMovie.getId());
    }

    @Query("SELECT * FROM Movie")
    public abstract Observable<List<Movie>> getAllMovieAsList();

    @Query("SELECT * FROM TvMovie")
    public abstract Observable<List<TvMovie>> getAllTvMovieAsList();

    @Query("SELECT * FROM GenreByMovie")
    public abstract Observable<List<GenreByMovie>> getAllGenreByMovieAsList();

    @Query("SELECT * FROM Movie WHERE _id = :id")
    public abstract Observable<Movie> getMovieAsObject(int id);
}
