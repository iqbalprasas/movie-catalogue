package com.prasas.moviecatalogue.utils;

import com.prasas.moviecatalogue.data.entity.Genre;

import java.util.List;

public interface OnGetGenreCallback {
    void onSuccess(List<Genre> genreList);
    void onError(Throwable throwable);
}
