package com.prasas.moviecatalogue.utils;

import android.view.View;
import android.widget.CompoundButton;

import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;

import java.util.HashMap;

public interface OnClickListener {
    interface MovieClick {
        void onMovieItemClick(Object movie, HashMap<String, View> viewHashMap);

        void onRefreshClick();

        void onFavoriteClick();
    }

    interface ListMovieClick {
        void onMovieItemClick(Movie movie);

        void onFavoriteItemClick(Movie movie, boolean isFavorite, int position);

        void onRefreshClick();
    }

    interface ListTvMovieClick {
        void onTvMovieItemClick(TvMovie tvMovie);

        void onFavoriteItemClick(TvMovie tvMovie, boolean isFavorite, int position);

        void onRefreshClick();
    }

    interface LanguageClick {
        void onSaveClick();

        void onRadioClick(int checkedRadio);
    }

    interface DailyNotificationClick {
        void onSwitchDailyReminderClick(CompoundButton compoundButton, boolean b);
        void onSwitchReleaseTodayReminderClick(CompoundButton compoundButton, boolean b);
    }

    interface FavoriteMovieClick {
        void onMovieItemClick(Movie movie);

        void onRemoveMovieClick(Movie movie, int position);
    }

    interface FavoriteTvMovieClick {
        void onTvMovieItemClick(TvMovie tvMovie);

        void onRemoveTvMovieClick(TvMovie tvMovie, int position);
    }

    interface MovieDetailClick {
        void onFavoriteClick(boolean isFavorite);
    }
}
