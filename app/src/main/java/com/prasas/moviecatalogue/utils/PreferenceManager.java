package com.prasas.moviecatalogue.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.prasas.moviecatalogue.data.entity.Genre;
import com.prasas.moviecatalogue.data.entity.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.prasas.moviecatalogue.utils.Constant.GENRE_KEY;
import static com.prasas.moviecatalogue.utils.Constant.LANGUAGE_KEY;

public class PreferenceManager {
    public static final String LANGUAGE_ENGLISH = "en";
    public static final String LANGUAGE_INDONESIA = "in";
    private static TinyDB tinyDB;

    public static Context setLocale(Context mContext) {
        if (tinyDB == null) {
            tinyDB = new TinyDB(mContext);
        }

        String currentLanguage;
        try {
            if (tinyDB.getString(LANGUAGE_KEY).equals("")) {
                tinyDB.putString(LANGUAGE_KEY, LANGUAGE_ENGLISH);
                currentLanguage = LANGUAGE_ENGLISH;
            } else {
                currentLanguage = tinyDB.getString(LANGUAGE_KEY);
            }
        } catch (Exception e) {
            tinyDB.putString(LANGUAGE_KEY, LANGUAGE_ENGLISH);
            currentLanguage = LANGUAGE_ENGLISH;
        }

        return updateResources(mContext, currentLanguage);
    }

    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.setLocale(locale);
        context = context.createConfigurationContext(config);

        return context;
    }

    public static void setAllGenre(List<Genre> genres) {
        ArrayList<Object> tempGenre = new ArrayList<>(genres);
        tinyDB.putListObject(GENRE_KEY, tempGenre);
    }

    public static void setAllGenreFromLocal() {
        if (getAllGenre().size() == 0) {
            ArrayList<Object> tempGenre = new ArrayList<>(getGenresFromJson());
            tinyDB.putListObject(GENRE_KEY, tempGenre);
        }
    }

    private static ArrayList<Object> getAllGenre() {
        return tinyDB.getListObject(GENRE_KEY, Genre.class);
    }

    public static String getGenreByMovie(Movie movie) {
        String genres = "";
        for (int genreId : movie.getGenre_ids()) {
            for (Object genre : getAllGenre()) {
                if (genreId == ((Genre) genre).getId()) {
                    if (genres.equals("")) {
                        genres = String.format("%s%s", genres, ((Genre) genre).getName());
                    } else {
                        genres = String.format("%s, %s", genres, ((Genre) genre).getName());
                    }
                }
            }
        }
        return genres;
    }

    private static ArrayList<Genre> getGenresFromJson() {
        return new Gson().fromJson(jsonGenre, new TypeToken<ArrayList<Genre>>() {
        }.getType());
    }

    private static String jsonGenre = "[\n" +
            "        {\n" +
            "            \"id\": 28,\n" +
            "            \"name\": \"Action\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 12,\n" +
            "            \"name\": \"Adventure\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 16,\n" +
            "            \"name\": \"Animation\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 35,\n" +
            "            \"name\": \"Comedy\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 80,\n" +
            "            \"name\": \"Crime\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 99,\n" +
            "            \"name\": \"Documentary\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 18,\n" +
            "            \"name\": \"Drama\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10751,\n" +
            "            \"name\": \"Family\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 14,\n" +
            "            \"name\": \"Fantasy\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 36,\n" +
            "            \"name\": \"History\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 27,\n" +
            "            \"name\": \"Horror\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10402,\n" +
            "            \"name\": \"Music\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 9648,\n" +
            "            \"name\": \"Mystery\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10749,\n" +
            "            \"name\": \"Romance\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 878,\n" +
            "            \"name\": \"Science Fiction\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10770,\n" +
            "            \"name\": \"TV MovieActivity\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 53,\n" +
            "            \"name\": \"Thriller\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10752,\n" +
            "            \"name\": \"War\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 37,\n" +
            "            \"name\": \"Western\"\n" +
            "        }\n" +
            "    ]";
}
