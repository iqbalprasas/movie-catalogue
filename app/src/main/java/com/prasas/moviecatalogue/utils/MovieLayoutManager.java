package com.prasas.moviecatalogue.utils;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;

public class MovieLayoutManager extends GridLayoutManager {
    private boolean isScroll = true;

    public MovieLayoutManager(Context context, int spanCount) {
        super(context, spanCount);
    }


    public void setScrollEnabled(boolean isScroll) {
        this.isScroll = isScroll;
    }

    @Override
    public boolean canScrollVertically() {
        return isScroll && super.canScrollVertically();
    }
}
