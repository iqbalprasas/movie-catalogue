package com.prasas.moviecatalogue.utils;

import androidx.transition.Explode;
import androidx.transition.Transition;

public class Constant {
    public static final int PAGE_SIZE = 20;
    public static int NUM_OF_SLIDE = 5;
    public static final String IS_LANGUAGE_CHANGED = "isLanguageChanged";
    public static final String LANGUAGE_KEY = "language";
    public static final String DAILY_REMINDER_KEY = "dailyReminder";
    public static final String RELEASE_TODAY_REMINDER_KEY = "releaseTodayReminder";
    public static final String GENRE_KEY = "genre";
    public static final String TYPE_DAILY_REMINDER = "DAILY_REMINDER";
    public static final String TYPE_RELEASE_TODAY_REMINDER = "RELEASE_TODAY_REMINDER";

    public static String CURSOR_DRAWABLE_RES = "mCursorDrawableRes";

    public static final String DB_NAME = "MovieActivity";
    public static final String TABLE_NAME_MOVIE = "Movie";
    public static final String TABLE_NAME_TVMOVIE = "TvMovie";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "TITLE";
    public static final String COLUMN_VOTE = "VOTE";
    public static final String COLUMN_VOTE_AVERAGE = "VOTE_AVERAGE";
    public static final String COLUMN_POSTER_PATH = "POSTER_PATH";
    public static final String COLUMN_BACKDROP_PATH = "BACKDROP_PATH";
    public static final String COLUMN_GENRE = "GENRE";
    public static final String COLUMN_ADULT = "ADULT";
    public static final String COLUMN_OVERVIEW = "OVERVIEW";
    public static final String COLUMN_RELEASE_DATE = "RELEASE_DATE";
    public static final String COLUMN_FAVORITE = "FAVORITE";
    public static final String COLUMN_VIDEO = "VIDEO";
    public static final String COLUMN_POPULARITY = "POPULARITY";
    public static final String COLUMN_LANGUAGE = "LANGUAGE";
    public static final String COLUMN_ORIGINAL_TITLE = "ORIGINAL_TITLE";

    public static int TOOLBAR_TYPE_EMPTY = 0;
    public static int TOOLBAR_TYPE_COLLAPSING = 1;
    public static int TOOLBAR_TYPE_DETAIL = 2;
    public static int TOOLBAR_TYPE_COLLAPSING_AND_DETAIL = 3;

    public static String STATE = "STATE";
    public static String STATE_ROTATED = "state_rotated";

    public static final String OPEN_MOVIE_FROM_WIDGET = "com.prasas.moviecatalogue.OPEN_MOVIE_FROM_WIDGET";
}
