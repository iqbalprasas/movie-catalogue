package com.prasas.moviecatalogue.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;

import com.prasas.moviecatalogue.R;

import java.lang.reflect.Field;

public class MovieSearchView extends SearchView {

    SearchView.SearchAutoComplete searchAutoComplete;
    OnQueryTextListener listener;
    private Path path;
    private Paint paint;

    public MovieSearchView(Context context) {
        super(context);
        init();
    }

    public MovieSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MovieSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        searchAutoComplete = this.findViewById(androidx.appcompat.R.id.search_src_text);
        searchAutoComplete.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        searchAutoComplete.setTextColor(getResources().getColor(R.color.colorPrimary));
        searchAutoComplete.setPadding(0, 0, 0, 0);
        searchAutoComplete.setDropDownBackgroundResource(R.drawable.frame_rounded_love);
        searchAutoComplete.setHintTextColor(getResources().getColor(R.color.colorPrimary));
        try {
            Field field = TextView.class.getDeclaredField(Constant.CURSOR_DRAWABLE_RES);
            field.setAccessible(true);
            field.set(searchAutoComplete, R.drawable.cursor);
        } catch (Exception e) {
            // Ignore exception
        }
        ImageView clearButton = this.findViewById(androidx.appcompat.R.id.search_close_btn);
        clearButton.setImageResource(R.drawable.ic_close_black);

        path = new Path();
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.WHITE);

        setBackgroundColor(Color.TRANSPARENT);
        setElevation(10.0f);
    }

    @Override
    public void setOnQueryTextListener(OnQueryTextListener listener) {
        super.setOnQueryTextListener(listener);
        this.listener = listener;
        searchAutoComplete.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (listener != null) {
                listener.onQueryTextSubmit(getQuery().toString());
            }
            return true;
        });
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        int searchViewHeight = getHeight();
        int searchViewSpace = (int) (searchViewHeight * 0.125);
        int searchViewWidth = getWidth();
        int searchViewRadius = (int) (getWidth() * 0.025);

        path.reset();
        path.moveTo(0, searchViewSpace + searchViewRadius);
        path.cubicTo(0, searchViewSpace + searchViewRadius,
                0, searchViewSpace,
                searchViewRadius, searchViewSpace);
        path.lineTo(searchViewWidth - searchViewRadius, searchViewSpace);
        path.cubicTo(searchViewWidth - searchViewRadius, searchViewSpace,
                searchViewWidth, searchViewSpace,
                searchViewWidth, searchViewSpace + searchViewRadius);
        path.lineTo(searchViewWidth, searchViewHeight - searchViewSpace - searchViewRadius);
        path.cubicTo(searchViewWidth, searchViewHeight - searchViewSpace - searchViewRadius,
                searchViewWidth, searchViewHeight - searchViewSpace,
                searchViewWidth - searchViewRadius, searchViewHeight - searchViewSpace);
        path.lineTo(searchViewRadius, searchViewHeight - searchViewSpace);
        path.cubicTo(searchViewRadius, searchViewHeight - searchViewSpace,
                0, searchViewHeight - searchViewSpace,
                0, searchViewHeight - searchViewSpace - searchViewRadius);

        path.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }
}
