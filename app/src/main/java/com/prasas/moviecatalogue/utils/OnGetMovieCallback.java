package com.prasas.moviecatalogue.utils;

public interface OnGetMovieCallback<T> {
    void onSuccess(T movieList);
    void onError(Throwable throwable);
}
