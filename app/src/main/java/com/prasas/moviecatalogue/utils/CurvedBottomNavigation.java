package com.prasas.moviecatalogue.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CurvedBottomNavigation extends BottomNavigationView {
    private Path path;
    private Paint paint;

    private Point firstCurveStartPoint;
    private Point firstCurveEndPoint;
    private Point secondCurveStartPoint;
    private Point secondCurveEndPoint;
    private Point firstCurveControlPoint1;
    private Point firstCurveControlPoint2;
    private Point secondCurveControlPoint1;
    private Point secondCurveControlPoint2;

    public CurvedBottomNavigation(Context context) {
        super(context);
        init();
    }

    public CurvedBottomNavigation(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CurvedBottomNavigation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        path = new Path();
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.WHITE);
        setBackgroundColor(Color.TRANSPARENT);

        firstCurveStartPoint = new Point();
        firstCurveEndPoint = new Point();
        secondCurveStartPoint = new Point();
        secondCurveEndPoint = new Point();
        firstCurveControlPoint1 = new Point();
        firstCurveControlPoint2 = new Point();
        secondCurveControlPoint1 = new Point();
        secondCurveControlPoint2 = new Point();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int navigationHeight = getHeight();
        int navigationWidth = getWidth();
        int CURVE_CIRCLE_RADIUS = navigationHeight / 2;
        firstCurveStartPoint.set((navigationWidth / 2) - (CURVE_CIRCLE_RADIUS * 2) - (CURVE_CIRCLE_RADIUS / 3), 0);
        firstCurveEndPoint.set(navigationWidth / 2, CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4));
        secondCurveStartPoint = firstCurveEndPoint;
        secondCurveEndPoint.set((navigationWidth / 2) + (CURVE_CIRCLE_RADIUS * 2) + (CURVE_CIRCLE_RADIUS / 3), 0);

        firstCurveControlPoint1.set(firstCurveStartPoint.x + CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4), firstCurveStartPoint.y);
        firstCurveControlPoint2.set(firstCurveEndPoint.x - (CURVE_CIRCLE_RADIUS * 2) + CURVE_CIRCLE_RADIUS, firstCurveEndPoint.y);
        secondCurveControlPoint1.set(secondCurveStartPoint.x + (CURVE_CIRCLE_RADIUS * 2) - CURVE_CIRCLE_RADIUS, secondCurveStartPoint.y);
        secondCurveControlPoint2.set(secondCurveEndPoint.x - (CURVE_CIRCLE_RADIUS + (CURVE_CIRCLE_RADIUS / 4)), secondCurveEndPoint.y);

        path.reset();
        path.moveTo(0, 0);
        path.lineTo(firstCurveStartPoint.x, firstCurveStartPoint.y);

        path.cubicTo(firstCurveControlPoint1.x, firstCurveControlPoint1.y,
                firstCurveControlPoint2.x, firstCurveControlPoint2.y,
                firstCurveEndPoint.x, firstCurveEndPoint.y);

        path.cubicTo(secondCurveControlPoint1.x, secondCurveControlPoint1.y,
                secondCurveControlPoint2.x, secondCurveControlPoint2.y,
                secondCurveEndPoint.x, secondCurveEndPoint.y);

        path.lineTo(navigationWidth, 0);
        path.lineTo(navigationWidth, navigationHeight);
        path.lineTo(0, navigationHeight);
        path.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }
}
