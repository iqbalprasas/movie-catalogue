package com.prasas.moviecatalogue.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;

import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.transition.Explode;
import androidx.transition.Transition;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Util {

    private static CircularProgressDrawable circularProgressDrawable;
    private static Transition transition;

    public static CircularProgressDrawable getcircularProgressDrawable(Context context){
        if (circularProgressDrawable == null){
            synchronized (CircularProgressDrawable.class) {
                if (circularProgressDrawable == null){
                    circularProgressDrawable = new CircularProgressDrawable(context);
                    circularProgressDrawable.setStrokeWidth(5f);
                    circularProgressDrawable.setColorSchemeColors(R.color.colorPrimary);
                    circularProgressDrawable.setCenterRadius(30f);
                    circularProgressDrawable.start();
                }
            }
        }
        return circularProgressDrawable;
    }

    public static Transition getExplodeTransition(){
        if (transition == null){
            synchronized (Transition.class){
                if (transition == null){
                    transition = new Explode();
                    transition.setDuration(100);
                }
            }
        }
        return transition;
    }

    public static String genreListToString(List<Integer> genreList){
        if (genreList == null){
            return "";
        }
        StringBuilder genreToString = new StringBuilder();
        for (int genreId : genreList){
            genreToString.append(genreId).append(",");
        }

        return genreToString.toString();
    }

    public static ArrayList<Integer> genreStringToList(String genreString){
        if (genreString == null){
            return new ArrayList<>();
        }

        String[] genreStringList = genreString.split(",");
        ArrayList<Integer> genreIntList = new ArrayList<>();
        for (String genre : genreStringList) {
            if (!genre.isEmpty()) {
                genreIntList.add(Integer.parseInt(genre));
            }
        }

        return genreIntList;
    }
}
