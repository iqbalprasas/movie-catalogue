package com.prasas.moviecatalogue.ui.listmovie;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.utils.BaseViewHolder;
import com.prasas.moviecatalogue.utils.OnClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TvMovieListAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<TvMovie> tvMovieList;
    private OnClickListener.ListTvMovieClick onItemClickListener;
    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;

    TvMovieListAdapter(OnClickListener.ListTvMovieClick onItemClickListener) {
        tvMovieList = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_NORMAL) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
            return new MoviesViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return tvMovieList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == tvMovieList.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    void setTvMovieList(List<TvMovie> tvMovieList) {
        this.tvMovieList = tvMovieList;
        notifyDataSetChanged();
    }

    void addLoading() {
        isLoaderVisible = true;
        TvMovie movie = new TvMovie();
        movie.setTitle("loading");
        tvMovieList.add(movie);
    }

    void removeLoading() {
        if (tvMovieList.size() != 0) {
            isLoaderVisible = false;
            int position = tvMovieList.size() - 1;
            TvMovie loading = tvMovieList.get(position);
            if (loading != null) {
                if (loading.getTitle().equals("loading")) {
                    tvMovieList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    public class MoviesViewHolder extends BaseViewHolder {
        @BindView(R.id.img_item_photo)
        ImageView imgPhoto;
        @BindView(R.id.tv_item_title)
        TextView txTitle;
        @BindView(R.id.tx_rating)
        TextView txRating;
        @BindView(R.id.btn_love_active)
        ImageView btnLoveActive;
        @BindView(R.id.btn_love_inactive)
        ImageView btnLoveInactive;
        @BindView(R.id.card_view)
        CardView cardView;
        View view;
        CircularProgressDrawable circularProgressDrawable;

        MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, itemView);

            circularProgressDrawable = new CircularProgressDrawable(itemView.getContext());
            circularProgressDrawable.setStrokeWidth(5f);
            circularProgressDrawable.setColorSchemeColors(R.color.colorPrimary);
            circularProgressDrawable.setCenterRadius(30f);
            circularProgressDrawable.start();

            HashMap<String, View> viewHashMap = new HashMap<>();
            viewHashMap.put("titleMovie", txTitle);
            viewHashMap.put("rankMovie", txRating);

            btnLoveActive.setOnClickListener((View view) -> onItemClickListener.onFavoriteItemClick(tvMovieList.get(getCurrentPosition()), true, getCurrentPosition()));
            btnLoveInactive.setOnClickListener((View view) -> onItemClickListener.onFavoriteItemClick(tvMovieList.get(getCurrentPosition()), false, getCurrentPosition()));
            cardView.setOnClickListener((View view) -> onItemClickListener.onTvMovieItemClick(tvMovieList.get(getCurrentPosition())));
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            TvMovie movie = tvMovieList.get(position);
            txTitle.setText(movie.getName());
            txRating.setText(String.valueOf(movie.getVoteAverage()));
            if (movie.isFavorite()) {
                btnLoveActive.setVisibility(View.VISIBLE);
                btnLoveInactive.setVisibility(View.GONE);
            } else {
                btnLoveActive.setVisibility(View.GONE);
                btnLoveInactive.setVisibility(View.VISIBLE);
            }
            Glide.with(view)
                    .load(BuildConfig.PHOTO_BASE_URL_LOW + movie.getPosterPath())
                    .centerCrop()
                    .placeholder(circularProgressDrawable)
                    .error(R.drawable.ic_image)
                    .into(imgPhoto);
        }
    }

    public class LoadingHolder extends BaseViewHolder {
        @BindView(R.id.progressbar_item)
        ProgressBar progressBar;

        LoadingHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }
}
