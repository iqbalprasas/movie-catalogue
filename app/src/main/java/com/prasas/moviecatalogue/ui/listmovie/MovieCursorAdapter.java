package com.prasas.moviecatalogue.ui.listmovie;


import android.content.Context;
import android.database.Cursor;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cursoradapter.widget.CursorAdapter;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;

public class MovieCursorAdapter extends CursorAdapter {

    private OnClickListener.MovieClick onItemClickListener;

    MovieCursorAdapter(Context context, OnClickListener.MovieClick onItemClickListener, Cursor cursor) {
        super(context, cursor, false);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_suggestion_movie, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        int id = cursor.getInt(cursor.getColumnIndexOrThrow(Constant.COLUMN_ID));
        String title = cursor.getString(cursor.getColumnIndexOrThrow(Constant.COLUMN_TITLE));
        int vote = cursor.getInt(cursor.getColumnIndexOrThrow(Constant.COLUMN_VOTE));
        float voteAverage = cursor.getFloat(cursor.getColumnIndexOrThrow(Constant.COLUMN_VOTE_AVERAGE));
        String imagePostrerUrl = cursor.getString(cursor.getColumnIndexOrThrow(Constant.COLUMN_POSTER_PATH));
        String imageBackdroprUrl = cursor.getString(cursor.getColumnIndexOrThrow(Constant.COLUMN_BACKDROP_PATH));
        String genre = cursor.getString(cursor.getColumnIndexOrThrow(Constant.COLUMN_GENRE));
        ArrayList<Integer> genreList = Util.genreStringToList(genre);
        boolean adult = cursor.getInt(cursor.getColumnIndexOrThrow(Constant.COLUMN_ADULT)) == 1;
        String overview = cursor.getString(cursor.getColumnIndexOrThrow(Constant.COLUMN_OVERVIEW));
        String releaseDate = cursor.getString(cursor.getColumnIndexOrThrow(Constant.COLUMN_RELEASE_DATE));
        boolean favorite = cursor.getInt(cursor.getColumnIndexOrThrow(Constant.COLUMN_FAVORITE)) == 1;

        TextView txTitle = view.findViewById(R.id.tx_title);
        ImageView imgPhoto = view.findViewById(R.id.img_item_photo);
        txTitle.setText(title);

        if (imageBackdroprUrl != null) {
            imgPhoto.setVisibility(View.VISIBLE);
            txTitle.setGravity(Gravity.START);
            CircularProgressDrawable circularProgressDrawable = Util.getcircularProgressDrawable(context);
            Glide.with(view)
                    .load(BuildConfig.PHOTO_BASE_URL_LOW + imageBackdroprUrl)
                    .centerCrop()
                    .placeholder(circularProgressDrawable)
                    .error(R.drawable.ic_image_black)
                    .into(imgPhoto);
        } else {
            txTitle.setGravity(Gravity.CENTER);
            imgPhoto.setVisibility(View.INVISIBLE);
        }

        HashMap<String, View> viewHashMap = new HashMap<>();
        viewHashMap.put("titleMovie", txTitle);

        if (MovieActivity.CURRENT_MENU == MovieActivity.POSITION_MOVIE) {
            Movie movie = new Movie();
            movie.setId(id);
            movie.setTitle(title);
            movie.setPosterPath(imagePostrerUrl);
            movie.setBackdrop_path(imageBackdroprUrl);
            movie.setGenre_ids(genreList);
            movie.setVoteCount(vote);
            movie.setVoteAverage(voteAverage);
            movie.setAdult(adult);
            movie.setOverview(overview);
            movie.setReleaseDate(releaseDate);
            movie.setFavorite(favorite);

            view.setOnClickListener((View v) -> onItemClickListener.onMovieItemClick(movie, viewHashMap));
        } else {
            TvMovie tvMovie = new TvMovie();
            tvMovie.setId(id);
            tvMovie.setName(title);
            tvMovie.setPosterPath(imagePostrerUrl);
            tvMovie.setBackdrop_path(imageBackdroprUrl);
            tvMovie.setGenre_ids(genreList);
            tvMovie.setVoteCount(vote);
            tvMovie.setVoteAverage(voteAverage);
            tvMovie.setAdult(adult);
            tvMovie.setOverview(overview);
            tvMovie.setReleaseDate(releaseDate);
            tvMovie.setFavorite(favorite);

            view.setOnClickListener((View v) -> onItemClickListener.onMovieItemClick(tvMovie, viewHashMap));
        }
    }
}
