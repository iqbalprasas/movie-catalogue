package com.prasas.moviecatalogue.ui.favoritemovie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseFragment;
import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.ui.detailmovie.MovieDetailActivity;
import com.prasas.moviecatalogue.utils.MovieLayoutManager;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;

public class FavoriteMovieListFragment extends BaseFragment<MovieViewModel> implements OnClickListener.FavoriteMovieClick {

    @BindView(R.id.recycler_movies)
    RecyclerView recyclerNews;
    @BindView(R.id.swipe_movies)
    SwipeRefreshLayout swipeMovies;
    @BindView(R.id.tx_message)
    TextView txMessage;

    private FavoriteMovieListAdapter movieListAdapter;
    private ArrayList<Movie> movieList;

    private MovieViewModel movieViewModel;

    public FavoriteMovieListFragment() {
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    public void initView() {
        initRecycler();
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
        movieViewModel.getFavoriteMovieList().observe(getViewLifecycleOwner(), movies -> {
            if (isShowMessage()) {
                hideMessage();
            }
            if (movies.size() == 0) {
                showMessage(Objects.requireNonNull(getActivity()).getString(R.string.no_favorite));
            }
            movieList.clear();
            movieList.addAll(movies);
        });
        movieViewModel.getFavoriteMovieWithGenreList().observe(getViewLifecycleOwner(), genreByMovieList -> {
            int i = 0;
            for (Movie m : movieList) {
                movieList.get(i).setGenre_ids(new ArrayList<>());
                for (GenreByMovie gbm : genreByMovieList) {
                    if (gbm.getIdMovie() == m.getId()) {
                        movieList.get(i).getGenre_ids().add(gbm.getIdGenre());
                    }
                }
                i++;
            }
            movieListAdapter.setMovieList(movieList);
            showLoading(false);
            setClickEnabled(true);
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movies;
    }

    @Override
    protected void onActivityCreated(Bundle instance, MovieViewModel viewModel) {
        initViewModel(viewModel);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        movieList = new ArrayList<>();

        initView();

        showLoading(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setClickEnabled(true);
    }

    private void initRecycler() {
        recyclerNews.setHasFixedSize(true);
        movieListAdapter = new FavoriteMovieListAdapter(this);
        MovieLayoutManager layoutManager = new MovieLayoutManager(getContext(), 2);
        recyclerNews.setLayoutManager(layoutManager);
        recyclerNews.setItemAnimator(new DefaultItemAnimator());
        recyclerNews.setAdapter(movieListAdapter);
        swipeMovies.setOnRefreshListener(() -> swipeMovies.setRefreshing(false));
    }

    private void showMovieDetail(Movie movie) {
        // goto movie detail
        Intent intent = new Intent(getContext(), MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.MOVIE_DETAIL, movie);
        startActivity(intent);
    }

    private void showMessage(String message) {
        swipeMovies.setVisibility(View.GONE);
        txMessage.setVisibility(View.VISIBLE);
        txMessage.setText(message);
    }

    private void hideMessage() {
        swipeMovies.setVisibility(View.VISIBLE);
        txMessage.setVisibility(View.INVISIBLE);
    }

    private boolean isShowMessage() {
        return (txMessage.getVisibility() == View.VISIBLE);
    }

    @Override
    public void onMovieItemClick(Movie movie) {
        if (isClickEnabled()) {
            setClickEnabled(false);
            showMovieDetail(movie);
        }
    }

    @Override
    public void onRemoveMovieClick(Movie movie, int position) {
        if (!isClickEnabled()) {
            return;
        }
        setClickEnabled(false);
        showLoading(true);
        movieViewModel.removeFavoriteMovie(movie);
        Toast.makeText(getContext(), Objects.requireNonNull(getContext()).getString(R.string.removed_from_favorite, movie.getTitle()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        movieViewModel.dispose(getContext());
    }
}
