package com.prasas.moviecatalogue.ui.dailynotification;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseActivity;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import butterknife.BindView;

public class DailyNotificationAcitvity extends BaseActivity<MovieViewModel> implements OnClickListener.DailyNotificationClick {

    private AlarmNotificationReceiver alarmNotificationReceiver;

    @BindView(R.id.switch_daily_reminder)
    SwitchCompat switchDailyReminder;
    @BindView(R.id.switch_daily_release_today)
    SwitchCompat switchReleaseTodayReminder;

    @Override
    protected void onCreate(Bundle instance, MovieViewModel viewModel) {
        initView();

        alarmNotificationReceiver = new AlarmNotificationReceiver();

        loadDailyReminderConfig();
        loadReleaseTodayReminderConfig();
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_notification_setting;
    }

    @Override
    protected int getToolbarType() {
        return Constant.TOOLBAR_TYPE_DETAIL;
    }

    @Override
    public void initView() {
        setTitle(getString(R.string.notification));

        switchDailyReminder.setOnCheckedChangeListener(this::onSwitchDailyReminderClick);
        switchReleaseTodayReminder.setOnCheckedChangeListener(this::onSwitchReleaseTodayReminderClick);
    }

    @Override
    public void onSwitchDailyReminderClick(CompoundButton compoundButton, boolean b) {
        if (!isClickEnabled()) return;

        setClickEnabled(false);

        if (b) {
            alarmNotificationReceiver.setDailyReminderAlarm(this);
            Toast.makeText(DailyNotificationAcitvity.this, getString(R.string.daily_reminder_on), Toast.LENGTH_SHORT).show();
        }else {
            alarmNotificationReceiver.cancelAlarm(this, Constant.TYPE_DAILY_REMINDER);
            Toast.makeText(DailyNotificationAcitvity.this, getString(R.string.daily_reminder_off), Toast.LENGTH_SHORT).show();
        }
        saveDailyReminderConfig();

        setClickEnabled(true);
    }

    @Override
    public void onSwitchReleaseTodayReminderClick(CompoundButton compoundButton, boolean b) {
        if (!isClickEnabled()) return;

        setClickEnabled(false);

        if (b) {
            alarmNotificationReceiver.setReleaseTodayReminderAlarm(this);
            Toast.makeText(DailyNotificationAcitvity.this, getString(R.string.release_today_reminder_on), Toast.LENGTH_SHORT).show();
        }else {
            alarmNotificationReceiver.cancelAlarm(this, Constant.TYPE_RELEASE_TODAY_REMINDER);
            Toast.makeText(DailyNotificationAcitvity.this, getString(R.string.release_today_reminder_off), Toast.LENGTH_SHORT).show();
        }
        saveReleaseTodayReminderConfig();

        setClickEnabled(true);
    }

    private void saveDailyReminderConfig(){
        if (switchDailyReminder.isChecked())
            tinyDB.putBoolean(Constant.DAILY_REMINDER_KEY, true);
        else
            tinyDB.putBoolean(Constant.DAILY_REMINDER_KEY, false);
    }

    private void saveReleaseTodayReminderConfig(){
        if (switchDailyReminder.isChecked())
            tinyDB.putBoolean(Constant.RELEASE_TODAY_REMINDER_KEY, true);
        else
            tinyDB.putBoolean(Constant.RELEASE_TODAY_REMINDER_KEY, false);
    }

    private void loadDailyReminderConfig(){
        setClickEnabled(false);

        if (tinyDB.getBoolean(Constant.DAILY_REMINDER_KEY))
            switchDailyReminder.setChecked(true);
        else
            switchDailyReminder.setChecked(false);

        setClickEnabled(true);
    }

    private void loadReleaseTodayReminderConfig(){
        setClickEnabled(false);

        if (tinyDB.getBoolean(Constant.RELEASE_TODAY_REMINDER_KEY))
            switchReleaseTodayReminder.setChecked(true);
        else
            switchReleaseTodayReminder.setChecked(false);

        setClickEnabled(true);
    }
}
