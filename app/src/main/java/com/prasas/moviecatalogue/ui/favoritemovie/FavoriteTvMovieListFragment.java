package com.prasas.moviecatalogue.ui.favoritemovie;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseFragment;
import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.ui.detailmovie.MovieDetailActivity;
import com.prasas.moviecatalogue.utils.MovieLayoutManager;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;

public class FavoriteTvMovieListFragment extends BaseFragment<MovieViewModel> implements OnClickListener.FavoriteTvMovieClick {

    @BindView(R.id.recycler_movies)
    RecyclerView recyclerNews;
    @BindView(R.id.swipe_movies)
    SwipeRefreshLayout swipeMovies;
    @BindView(R.id.tx_message)
    TextView txMessage;

    private FavoriteTvMovieListAdapter tvMovieListAdapter;
    private ArrayList<TvMovie> tvMovieList;

    private MovieViewModel movieViewModel;

    public FavoriteTvMovieListFragment() {
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    public void initView() {
        initRecycler();
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
        movieViewModel.getFavoriteTvMovieList().observe(getViewLifecycleOwner(), tvMovies -> {
            if (isShowMessage()) {
                hideMessage();
            }
            if (tvMovies.size() == 0) {
                showMessage(Objects.requireNonNull(getActivity()).getString(R.string.no_favorite));
            }
            tvMovieList.clear();
            tvMovieList.addAll(tvMovies);
        });
        movieViewModel.getFavoriteTvMovieWithGenreList().observe(getViewLifecycleOwner(), genreByMovieList -> {
            int i = 0;
            for (TvMovie tvm : tvMovieList) {
                tvMovieList.get(i).setGenre_ids(new ArrayList<>());
                for (GenreByMovie gbm : genreByMovieList) {
                    if (gbm.getIdMovie() == tvm.getId()) {
                        tvMovieList.get(i).getGenre_ids().add(gbm.getIdGenre());
                    }
                }
                i++;
            }
            tvMovieListAdapter.setTvMovieList(tvMovieList);
            showLoading(false);
            setClickEnabled(true);
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movies;
    }

    @Override
    protected void onActivityCreated(Bundle instance, MovieViewModel viewModel) {
        initViewModel(viewModel);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvMovieList = new ArrayList<>();

        initView();

        showLoading(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setClickEnabled(true);
    }

    private void initRecycler() {
        recyclerNews.setHasFixedSize(true);
        tvMovieListAdapter = new FavoriteTvMovieListAdapter(this);
        MovieLayoutManager layoutManager = new MovieLayoutManager(getContext(), 2);
        recyclerNews.setLayoutManager(layoutManager);
        recyclerNews.setItemAnimator(new DefaultItemAnimator());
        recyclerNews.setAdapter(tvMovieListAdapter);
        swipeMovies.setOnRefreshListener(() -> swipeMovies.setRefreshing(false));
    }

    private void showTvMovieDetail(TvMovie tvMovie) {
        // goto tvmovie detail
        Intent intent = new Intent(getContext(), MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.TVMOVIE_DETAIL, tvMovie);
        startActivity(intent);
    }

    private void showMessage(String message) {
        swipeMovies.setVisibility(View.GONE);
        txMessage.setVisibility(View.VISIBLE);
        txMessage.setText(message);
    }

    private void hideMessage() {
        swipeMovies.setVisibility(View.VISIBLE);
        txMessage.setVisibility(View.INVISIBLE);
    }

    private boolean isShowMessage() {
        return (txMessage.getVisibility() == View.VISIBLE);
    }

    @Override
    public void onTvMovieItemClick(TvMovie tvMovie) {
        if (isClickEnabled()) {
            setClickEnabled(false);
            showTvMovieDetail(tvMovie);
        }
    }

    @Override
    public void onRemoveTvMovieClick(TvMovie tvMovie, int position) {
        if (!isClickEnabled()) {
            return;
        }
        setClickEnabled(false);
        showLoading(true);
        movieViewModel.removeFavoriteTvMovie(tvMovie);
        Toast.makeText(getContext(), Objects.requireNonNull(getContext()).getString(R.string.removed_from_favorite, tvMovie.getName()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        movieViewModel.dispose(getContext());
    }
}
