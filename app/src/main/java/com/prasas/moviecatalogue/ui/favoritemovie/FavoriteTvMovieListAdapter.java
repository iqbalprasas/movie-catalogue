package com.prasas.moviecatalogue.ui.favoritemovie;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.utils.BaseViewHolder;
import com.prasas.moviecatalogue.utils.OnClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteTvMovieListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ArrayList<TvMovie> tvMovieList;
    private OnClickListener.FavoriteTvMovieClick onItemClickListener;

    FavoriteTvMovieListAdapter(OnClickListener.FavoriteTvMovieClick onItemClickListener) {
        tvMovieList = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite_movie, parent, false);
        return new TvMoviesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return tvMovieList.size();
    }

    void setTvMovieList(ArrayList<TvMovie> tvMovieList) {
        this.tvMovieList = tvMovieList;
        notifyDataSetChanged();
    }

    public class TvMoviesHolder extends BaseViewHolder {
        @BindView(R.id.img_item_photo)
        ImageView imgPhoto;
        @BindView(R.id.tv_item_title)
        TextView txTitle;
        @BindView(R.id.tx_rating)
        TextView txRating;
        @BindView(R.id.btn_remove)
        ImageView btnRemove;
        @BindView(R.id.card_view)
        CardView cardView;
        View view;
        CircularProgressDrawable circularProgressDrawable;

        TvMoviesHolder(@NonNull View itemView) {
            super(itemView);

            this.view = itemView;
            ButterKnife.bind(this, itemView);

            circularProgressDrawable = new CircularProgressDrawable(itemView.getContext());
            circularProgressDrawable.setStrokeWidth(5f);
            circularProgressDrawable.setColorSchemeColors(R.color.colorPrimary);
            circularProgressDrawable.setCenterRadius(30f);
            circularProgressDrawable.start();

            btnRemove.setOnClickListener((View view) -> onItemClickListener.onRemoveTvMovieClick(tvMovieList.get(getCurrentPosition()), getCurrentPosition()));
            cardView.setOnClickListener((View view) -> onItemClickListener.onTvMovieItemClick(tvMovieList.get(getCurrentPosition())));
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            TvMovie tvMovie = tvMovieList.get(position);
            txTitle.setText(tvMovie.getTitle());
            txRating.setText(String.valueOf(tvMovie.getVoteAverage()));
            Glide.with(view)
                    .load(BuildConfig.PHOTO_BASE_URL_LOW + tvMovie.getPosterPath())
                    .centerCrop()
                    .placeholder(circularProgressDrawable)
                    .error(R.drawable.ic_image)
                    .into(imgPhoto);
        }
    }
}
