package com.prasas.moviecatalogue.ui.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseActivity;
import com.prasas.moviecatalogue.data.entity.Genre;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.ui.listmovie.MovieActivity;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.OnGetGenreCallback;
import com.prasas.moviecatalogue.utils.PreferenceManager;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import java.util.List;

public class SplashScreenActivity extends BaseActivity<MovieViewModel> {
    private MovieViewModel movieViewModel;
    private Movie movie;

    @Override
    protected void onCreate(Bundle instance, MovieViewModel viewModel) {
        PreferenceManager.setLocale(this);
        initViewModel(viewModel);

        if (getIntent().hasExtra(Constant.OPEN_MOVIE_FROM_WIDGET)) {
            int id = getIntent().getIntExtra(Constant.OPEN_MOVIE_FROM_WIDGET, 0);
            movie = movieViewModel.getMovie(id);
        }

        getGenres(this);
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected int getToolbarType() {
        return Constant.TOOLBAR_TYPE_EMPTY;
    }

    private void getGenres(Context context) {
        movieViewModel.getGenreList(context, new OnGetGenreCallback() {
            @Override
            public void onSuccess(List<Genre> genreList) {
                if (genreList != null) PreferenceManager.setAllGenre(genreList);
                else PreferenceManager.setAllGenreFromLocal();

                Intent intent = new Intent(SplashScreenActivity.this, MovieActivity.class);
                if (getIntent().hasExtra(Constant.OPEN_MOVIE_FROM_WIDGET))
                    intent.putExtra(Constant.OPEN_MOVIE_FROM_WIDGET, movie);

                startActivity(intent);
                finish();
            }

            @Override
            public void onError(Throwable throwable) {
                PreferenceManager.setAllGenreFromLocal();
                Intent intent = new Intent(SplashScreenActivity.this, MovieActivity.class);
                if (getIntent().hasExtra(Constant.OPEN_MOVIE_FROM_WIDGET))
                    intent.putExtra(Constant.OPEN_MOVIE_FROM_WIDGET, movie);

                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void initView() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        movieViewModel.dispose(this);
    }
}
