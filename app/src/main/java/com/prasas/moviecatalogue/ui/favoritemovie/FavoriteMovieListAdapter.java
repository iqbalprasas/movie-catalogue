package com.prasas.moviecatalogue.ui.favoritemovie;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.utils.BaseViewHolder;
import com.prasas.moviecatalogue.utils.OnClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteMovieListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ArrayList<Movie> movieList;
    private OnClickListener.FavoriteMovieClick onItemClickListener;

    FavoriteMovieListAdapter(OnClickListener.FavoriteMovieClick onItemClickListener) {
        movieList = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite_movie, parent, false);
        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void setMovieList(ArrayList<Movie> movieList) {
        this.movieList = movieList;
        notifyDataSetChanged();
    }

    public class MoviesViewHolder extends BaseViewHolder {
        @BindView(R.id.img_item_photo)
        ImageView imgPhoto;
        @BindView(R.id.tv_item_title)
        TextView txTitle;
        @BindView(R.id.tx_rating)
        TextView txRating;
        @BindView(R.id.btn_remove)
        ImageView btnRemove;
        @BindView(R.id.card_view)
        CardView cardView;
        View view;
        CircularProgressDrawable circularProgressDrawable;

        MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, itemView);

            circularProgressDrawable = new CircularProgressDrawable(itemView.getContext());
            circularProgressDrawable.setStrokeWidth(5f);
            circularProgressDrawable.setColorSchemeColors(R.color.colorPrimary);
            circularProgressDrawable.setCenterRadius(30f);
            circularProgressDrawable.start();

            btnRemove.setOnClickListener((View view) -> onItemClickListener.onRemoveMovieClick(movieList.get(getCurrentPosition()), getCurrentPosition()));
            cardView.setOnClickListener((View view) -> onItemClickListener.onMovieItemClick(movieList.get(getCurrentPosition())));
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            Movie movie = movieList.get(position);
            txTitle.setText(movie.getTitle());
            txRating.setText(String.valueOf(movie.getVoteAverage()));
            Glide.with(view)
                    .load(BuildConfig.PHOTO_BASE_URL_LOW + movie.getPosterPath())
                    .centerCrop()
                    .placeholder(circularProgressDrawable)
                    .error(R.drawable.ic_image)
                    .into(imgPhoto);
        }
    }

    public class LoadingHolder extends BaseViewHolder {
        @BindView(R.id.progressbar_item)
        ProgressBar progressBar;

        public LoadingHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }
}
