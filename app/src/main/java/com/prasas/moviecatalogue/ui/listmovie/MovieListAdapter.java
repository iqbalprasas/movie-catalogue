package com.prasas.moviecatalogue.ui.listmovie;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.transition.Fade;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.utils.BaseViewHolder;
import com.prasas.moviecatalogue.utils.OnClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<Movie> movieList;
    private OnClickListener.ListMovieClick onItemClickListener;
    static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;

    MovieListAdapter(OnClickListener.ListMovieClick onItemClickListener) {
        movieList = new ArrayList<>();
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_NORMAL) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
            return new MoviesViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == movieList.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
        notifyDataSetChanged();
    }

    void addLoading() {
        isLoaderVisible = true;
        Movie movie = new Movie();
        movie.setTitle("loading");
        movieList.add(movie);
    }

    void removeLoading() {
        if (movieList.size() != 0) {
            isLoaderVisible = false;
            int position = movieList.size() - 1;
            Movie loading = movieList.get(position);
            if (loading != null) {
                if (loading.getTitle().equals("loading")) {
                    movieList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    public class MoviesViewHolder extends BaseViewHolder {
        @BindView(R.id.img_item_photo)
        ImageView imgPhoto;
        @BindView(R.id.tv_item_title)
        TextView txTitle;
        @BindView(R.id.tx_rating)
        TextView txRating;
        @BindView(R.id.btn_love_active)
        ImageView btnLoveActive;
        @BindView(R.id.btn_love_inactive)
        ImageView btnLoveInactive;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.container_movie)
        ConstraintLayout constraintMovie;
        View view;
        CircularProgressDrawable circularProgressDrawable;

        MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, itemView);

            circularProgressDrawable = new CircularProgressDrawable(itemView.getContext());
            circularProgressDrawable.setStrokeWidth(5f);
            circularProgressDrawable.setColorSchemeColors(R.color.colorPrimary);
            circularProgressDrawable.setCenterRadius(30f);
            circularProgressDrawable.start();

            btnLoveActive.setOnClickListener((View view) -> onItemClickListener.onFavoriteItemClick(movieList.get(getCurrentPosition()), true, getCurrentPosition()));
            btnLoveInactive.setOnClickListener((View view) -> onItemClickListener.onFavoriteItemClick(movieList.get(getCurrentPosition()), false, getCurrentPosition()));
            cardView.setOnClickListener((View view) -> onItemClickListener.onMovieItemClick(movieList.get(getCurrentPosition())));
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            Movie movie = movieList.get(position);
            txTitle.setText(movie.getTitle());
            txRating.setText(String.valueOf(movie.getVoteAverage()));
            TransitionSet set = new TransitionSet()
                    .addTransition(new Fade())
                    .setInterpolator(new FastOutLinearInInterpolator());
            TransitionManager.beginDelayedTransition(constraintMovie, set);
            if (movie.isFavorite()) {
                btnLoveActive.setVisibility(View.VISIBLE);
                btnLoveInactive.setVisibility(View.GONE);
            } else {
                btnLoveActive.setVisibility(View.GONE);
                btnLoveInactive.setVisibility(View.VISIBLE);
            }
            Glide.with(view)
                    .load(BuildConfig.PHOTO_BASE_URL_LOW + movie.getPosterPath())
                    .centerCrop()
                    .placeholder(circularProgressDrawable)
                    .error(R.drawable.ic_image)
                    .into(imgPhoto);
        }

        public TextView getTxTitle() {
            return txTitle;
        }
    }

    public class LoadingHolder extends BaseViewHolder {
        @BindView(R.id.progressbar_item)
        ProgressBar progressBar;

        LoadingHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }
    }
}
