package com.prasas.moviecatalogue.ui.listmovie;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseFragment;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.ui.detailmovie.MovieDetailActivity;
import com.prasas.moviecatalogue.utils.MovieLayoutManager;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.utils.PaginationScrollListener;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;
import com.prasas.moviecatalogue.widget.WidgetWorker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

public class MovieListFragment extends BaseFragment<MovieViewModel> implements OnClickListener.ListMovieClick {

    @BindView(R.id.recycler_movies)
    RecyclerView recyclerMovie;
    @BindView(R.id.swipe_movies)
    SwipeRefreshLayout swipeMovies;
    @BindView(R.id.tx_message)
    TextView txMessage;
    @BindView(R.id.btn_refresh)
    ImageButton btnRefresh;

    private MovieListAdapter movieListAdapter;
    private List<Movie> idFavoriteList;
    private List<Movie> movieList;
    private boolean isLoadingMoreMovie = false;
    private boolean isLastPage = false;
    private static int PAGE = 1;

    private WorkManager workManager;
    private OneTimeWorkRequest oneTimeWorkRequest;

    private MovieViewModel movieViewModel;

    public MovieListFragment() {
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movies;
    }

    @Override
    protected void onActivityCreated(Bundle instance, MovieViewModel viewModel) {
        idFavoriteList = new ArrayList<>();
        movieList = new ArrayList<>();

        if (getActivity() != null) {
            workManager = WorkManager.getInstance(getActivity());
            oneTimeWorkRequest = new OneTimeWorkRequest.Builder(WidgetWorker.class).build();
        }

        initViewModel(viewModel);

        if (instance == null) {
            movieViewModel.getMovieListFromRemote(PAGE);
        }
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
        movieViewModel.getMovieListLiveData().observe(getViewLifecycleOwner(), movies -> {
            if (isShowMessage()) {
                hideMessage();
            }
            swipeMovies.setRefreshing(false);
            movieList = movies;
            syncAllMovieListWithFavorite();
        });
        movieViewModel.getMessageMovieList().observe(getViewLifecycleOwner(), message -> {
            showLoading(false);
            if (message.isError()) {
                showMessage(message.getMessage());
            }
        });
        movieViewModel.getFavoriteMovieList().observe(getViewLifecycleOwner(), movies -> {
            idFavoriteList = movies;
            if (movieList.size() != 0) {
                syncAllMovieListWithFavorite();
            }
            if (!isClickEnabled()) {
                setClickEnabled(true);
                showLoading(false);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();

        showLoading(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setClickEnabled(true);
    }

    private void initRecycler() {
        recyclerMovie.setHasFixedSize(true);
        movieListAdapter = new MovieListAdapter(this);
        MovieLayoutManager layoutManager = new MovieLayoutManager(getContext(), 2);
        layoutManager.setSpanSizeLookup(new MovieLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // show loading in a row
                // show movie in 2 row
                if (movieListAdapter.getItemViewType(position) == MovieListAdapter.VIEW_TYPE_LOADING) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        recyclerMovie.setLayoutManager(layoutManager);
        recyclerMovie.setItemAnimator(new DefaultItemAnimator());
        recyclerMovie.setAdapter(movieListAdapter);
        recyclerMovie.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                recyclerMovie.post(() -> movieListAdapter.notifyItemInserted(movieListAdapter.getItemCount()));
                showMoreMovies();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoadingMoreMovie;
            }
        });
        swipeMovies.setOnRefreshListener(() -> {
            isLoadingMoreMovie = false;
            isLastPage = false;
            swipeMovies.setRefreshing(false);
        });
    }

    private void showMovieDetail(Movie movie) {
        // goto movie detail
        Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.MOVIE_DETAIL, movie);

        startActivity(intent);
    }


    @Override
    public void initView() {
        initRecycler();

        btnRefresh.setOnClickListener((View v) -> this.onRefreshClick());
        swipeMovies.setOnRefreshListener(() -> movieViewModel.getMovieListFromRemote(PAGE));
    }

    private void showMoreMovies() {
        // add dummy loading
        movieListAdapter.addLoading();
        isLoadingMoreMovie = true;
        // show loading in 3 seconds
        new Handler().postDelayed(() -> {
            isLoadingMoreMovie = false;
            movieListAdapter.removeLoading();
        }, 3000);
    }

    private void syncAllMovieListWithFavorite() {
        int i = 0;
        for (Movie m : movieList) {
            movieList.get(i).setFavorite(false);
            for (Movie fm : idFavoriteList) {
                if (m.equalsId(fm)) {
                    movieList.get(i).setFavorite(true);
                }
            }
            i++;
        }
        movieListAdapter.setMovieList(movieList);
        startPostponedEnterTransition();
    }

    private void showMessage(String message) {
        swipeMovies.setVisibility(View.GONE);
        txMessage.setVisibility(View.VISIBLE);
        btnRefresh.setVisibility(View.VISIBLE);
        txMessage.setText(message);
    }

    private void hideMessage() {
        swipeMovies.setVisibility(View.VISIBLE);
        txMessage.setVisibility(View.INVISIBLE);
        btnRefresh.setVisibility(View.INVISIBLE);
    }

    private boolean isShowMessage() {
        return (txMessage.getVisibility() == View.VISIBLE);
    }

    @Override
    public void onMovieItemClick(Movie movie) {
        if (isClickEnabled()) {
            setClickEnabled(false);
            showMovieDetail(movie);
        }
    }

    @Override
    public void onFavoriteItemClick(Movie movie, boolean isFavorite, int position) {
        if (!isClickEnabled()) {
            return;
        }
        setClickEnabled(false);
        showLoading(true);

        if (workManager != null && oneTimeWorkRequest != null) {
            workManager.enqueueUniqueWork("widget_worker", ExistingWorkPolicy.REPLACE, oneTimeWorkRequest);
        }

        if (isFavorite) {
            movieViewModel.removeFavoriteMovie(movie);
            Toast.makeText(getContext(), Objects.requireNonNull(getContext()).getString(R.string.removed_from_favorite, movie.getTitle()), Toast.LENGTH_SHORT).show();
        } else {
            movieViewModel.addFavoriteMovie(movie);
            Toast.makeText(getContext(), Objects.requireNonNull(getContext()).getString(R.string.added_to_favorite, movie.getTitle()), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefreshClick() {
        hideMessage();
        showLoading(true);
        movieViewModel.getMovieListFromRemote(PAGE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        movieViewModel.dispose(getContext());
    }
}
