package com.prasas.moviecatalogue.ui.listmovie;

import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseActivity;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.ui.dailynotification.DailyNotificationAcitvity;
import com.prasas.moviecatalogue.ui.detailmovie.MovieDetailActivity;
import com.prasas.moviecatalogue.ui.favoritemovie.FavoriteMovieActivity;
import com.prasas.moviecatalogue.ui.language.LanguageActivity;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.CurvedBottomNavigation;
import com.prasas.moviecatalogue.utils.MovieSearchView;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.utils.PagerListAdapter;
import com.prasas.moviecatalogue.utils.Util;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static com.prasas.moviecatalogue.utils.Constant.IS_LANGUAGE_CHANGED;
import static com.prasas.moviecatalogue.utils.Constant.NUM_OF_SLIDE;

public class MovieActivity extends BaseActivity<MovieViewModel> implements OnClickListener.MovieClick,
        BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener, MovieSearchView.OnQueryTextListener {
    @BindView(R.id.collapsing_list_movie)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.appbar_list_movie)
    AppBarLayout appBarLayout;
    @BindView(R.id.view_pager_slide)
    ViewPager viewPagerSlide;
    @BindView(R.id.slider_dots)
    LinearLayout sliderDots;
    @BindView(R.id.progress_slide)
    ProgressBar progressSlide;
    @BindView(R.id.view_pager_list)
    ViewPager viewPagerTab;
    @BindView(R.id.frame_slide)
    FrameLayout frameSlide;
    @BindView(R.id.btn_refresh)
    ImageButton btnRefresh;
    @BindView(R.id.bottom_app_menu)
    CurvedBottomNavigation bottomNavigation;
    @BindView(R.id.fab_favorite)
    FloatingActionButton fabFavorite;

    private MovieSearchView movieSearchView;
    private MovieCursorAdapter movieCursorAdapter;

    private MovieViewModel movieViewModel;

    private int dotscount;
    private ImageView[] dots;
    private MovieSlidePagerAdapter movieSlidePagerAdapter;
    private int prevMenuPosition = 0;

    private final static int PAGE = 1;
    public static int CURRENT_MENU = 0;
    public final static int POSITION_MOVIE = 0;
    public final static int POSITION_TVMOVIE = 2;

    @Override
    protected void onCreate(Bundle instance, MovieViewModel viewModel) {
        if (getIntent().hasExtra(Constant.OPEN_MOVIE_FROM_WIDGET)) {
            Movie movie = getIntent().getParcelableExtra(Constant.OPEN_MOVIE_FROM_WIDGET);
            showMovieDetail(movie);
        }

        tinyDB.putBoolean(IS_LANGUAGE_CHANGED, false);
        setTitle(" ");

        initView();

        initViewModel(viewModel);

        showLoadingSlide(true);

        if (instance == null) {
            movieViewModel.getNowPlayingMovieListFromRemote(PAGE);
        }
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    public void initView() {
        bottomNavigation.inflateMenu(R.menu.bottom_menu);
        bottomNavigation.setOnNavigationItemSelectedListener(this);
        fabFavorite.setOnClickListener(view -> this.onFavoriteClick());
        btnRefresh.setOnClickListener((View view) -> this.onRefreshClick());

        initToolbar();
        initTabs();
        initSlide(NUM_OF_SLIDE);

        movieCursorAdapter = new MovieCursorAdapter(this, this, getSuggestionCursor(new ArrayList<Movie>()));
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
        movieViewModel.setupNowPlayingMovieListObservable(this);
        movieViewModel.setupMovieListObservable(this);
        movieViewModel.setupTvMovieListObservable(this);
        movieViewModel.setupSearchMovieListObservable(this);
        movieViewModel.setupSearchTvMovieListObservable(this);
        movieViewModel.getNowPlayingMovieListLiveData().observe(this, movies -> {
            if (isRefresh()) {
                hideRefresh();
            }
            movieSlidePagerAdapter.setContentSlide(movies);
        });
        movieViewModel.getMessageNowPlayingMovieList().observe(this, message -> {
            showLoadingSlide(false);
            if (message.isError()) {
                showRefresh();
            }
        });
        movieViewModel.getSearchMovieListLiveData().observe(this, movies -> {
            Cursor cursor = getSuggestionCursor(movies);
            movieCursorAdapter.swapCursor(cursor);
            movieCursorAdapter.notifyDataSetChanged();
        });
        movieViewModel.getSearchTvMovieListLiveData().observe(this, tvMovies -> {
            Cursor cursor = getSuggestionCursor(tvMovies);
            movieCursorAdapter.swapCursor(cursor);
            movieCursorAdapter.notifyDataSetChanged();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (tinyDB.getBoolean(IS_LANGUAGE_CHANGED)) {
            recreate();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_movies;
    }

    @Override
    protected int getToolbarType() {
        return Constant.TOOLBAR_TYPE_COLLAPSING;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        movieSearchView = (MovieSearchView) searchItem.getActionView();
        movieSearchView.setOnQueryTextListener(this);
        movieSearchView.setQueryHint(String.format("%s%s%s%s", getString(R.string.search), " ", getString(R.string.movies), "..."));

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                Transition transition = Util.getExplodeTransition();
                transition.addTarget(movieSearchView);
                TransitionManager.beginDelayedTransition(MovieActivity.this.findViewById(R.id.toolbar_movie), transition);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                Transition transition = Util.getExplodeTransition();
                transition.addTarget(movieSearchView);
                TransitionManager.beginDelayedTransition(MovieActivity.this.findViewById(R.id.toolbar_movie), transition);
                return true;
            }
        });
        movieSearchView.setSuggestionsAdapter(movieCursorAdapter);

        return true;
    }

    private Cursor getSuggestionCursor(Object movieList) {
        String[] menuCols = new String[]{
                Constant.COLUMN_ID, Constant.COLUMN_TITLE, Constant.COLUMN_VOTE,
                Constant.COLUMN_VOTE_AVERAGE, Constant.COLUMN_POSTER_PATH, Constant.COLUMN_BACKDROP_PATH, Constant.COLUMN_GENRE, Constant.COLUMN_ADULT,
                Constant.COLUMN_OVERVIEW, Constant.COLUMN_RELEASE_DATE, Constant.COLUMN_FAVORITE
        };

        MatrixCursor cursor = new MatrixCursor(menuCols);

        if (((List<?>) movieList).size() == 0) {
            cursor.addRow(new Object[]{0, getString(R.string.no_results), null, null, null, null, null, null, null, null, null});
        } else {
            if (CURRENT_MENU == POSITION_MOVIE) {
                for (Object m : (List<?>) movieList) {
                    Movie movie = (Movie) m;
                    String genreString = Util.genreListToString(movie.getGenre_ids());

                    cursor.addRow(new Object[]{
                            movie.getId(), movie.getTitle(), movie.getVoteCount(), movie.getVoteAverage(), movie.getPosterPath(),
                            movie.getBackdrop_path(), genreString, (movie.isAdult() ? 1 : 0), movie.getOverview(),
                            movie.getReleaseDate(), (movie.isFavorite() ? 1 : 0)
                    });
                }
            } else {
                for (Object tvM : (List<?>) movieList) {
                    TvMovie tvMovie = (TvMovie) tvM;
                    String genreString = Util.genreListToString(tvMovie.getGenre_ids());

                    cursor.addRow(new Object[]{
                            tvMovie.getId(), tvMovie.getName(), tvMovie.getVoteCount(), tvMovie.getVoteAverage(), tvMovie.getPosterPath(),
                            tvMovie.getBackdrop_path(), genreString, (tvMovie.isAdult() ? 1 : 0), tvMovie.getOverview(),
                            tvMovie.getReleaseDate(), (tvMovie.isFavorite() ? 1 : 0)
                    });
                }
            }
        }

        return cursor;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_language:
                startActivity(new Intent(MovieActivity.this, LanguageActivity.class));
                return true;
            case R.id.menu_daily_notification:
                startActivity(new Intent(MovieActivity.this, DailyNotificationAcitvity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (scrollRange == -1) scrollRange = appBarLayout.getTotalScrollRange();

                if (scrollRange + i == 0) {
                    collapsingToolbarLayout.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void initSlide(int limit) {
        movieSlidePagerAdapter = new MovieSlidePagerAdapter(this);
        ArrayList<Movie> movieList = new ArrayList<>();
        movieSlidePagerAdapter.setContentSlide(movieList);
        movieSlidePagerAdapter.setOnClickListener(this);
        viewPagerSlide.setAdapter(movieSlidePagerAdapter);

        dotscount = limit;
        dots = new ImageView[dotscount];
        for (int i = 0; i < dotscount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.frame_non_active_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDots.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.frame_active_dot));

        viewPagerSlide.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.frame_non_active_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.frame_active_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initTabs() {
        PagerListAdapter pagerAdapter = new PagerListAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new MovieListFragment(), getString(R.string.movies));
        pagerAdapter.addFragment(new TvMovieListFragment(), getString(R.string.tv_movies));
        viewPagerTab.setAdapter(pagerAdapter);
        viewPagerTab.addOnPageChangeListener(this);
        CURRENT_MENU = POSITION_MOVIE;
    }

    public void showMovieDetail(Movie movie, HashMap<String, View> viewHashMap) {
        // goto movie detail
        Intent intent = new Intent(MovieActivity.this, MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.MOVIE_DETAIL, movie);

        Pair<View, String> pairTitle = Pair.create(viewHashMap.get("titleMovie"), "titleMovie");

        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairTitle);
        startActivity(intent, activityOptionsCompat.toBundle());
    }

    public void showMovieDetail(Movie movie) {
        Intent intent = new Intent(MovieActivity.this, MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.MOVIE_DETAIL, movie);

        startActivity(intent);
    }

    private void showTvMovieDetail(TvMovie tvMovie, HashMap<String, View> viewHashMap) {
        // goto tvmovie detail
        Intent intent = new Intent(MovieActivity.this, MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.TVMOVIE_DETAIL, tvMovie);

        Pair<View, String> pairTitle = Pair.create(viewHashMap.get("titleMovie"), "titleMovie");

        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairTitle);
        startActivity(intent, activityOptionsCompat.toBundle());
    }

    private void showLoadingSlide(boolean isShow) {
        progressSlide.setVisibility(isShow
                ? View.VISIBLE
                : View.GONE);
    }

    private void showRefresh() {
        frameSlide.setVisibility(View.GONE);
        btnRefresh.setVisibility(View.VISIBLE);
    }

    private void hideRefresh() {
        frameSlide.setVisibility(View.VISIBLE);
        btnRefresh.setVisibility(View.GONE);
    }

    private boolean isRefresh() {
        return (btnRefresh.getVisibility() == View.VISIBLE);
    }

    private void clearSearch() {
        if (CURRENT_MENU == POSITION_MOVIE) movieSearchView.setQueryHint(String.format("%s%s%s%s", getString(R.string.search), " ", getString(R.string.movies), "..."));
        else movieSearchView.setQueryHint(String.format("%s%s%s%s", getString(R.string.search), " ", getString(R.string.tv_movies), "..."));

        Cursor cursor = getSuggestionCursor(new ArrayList<Movie>());
        movieCursorAdapter.swapCursor(cursor);
        movieCursorAdapter.notifyDataSetChanged();

        movieSearchView.clearFocus();
        movieSearchView.setQuery("", false);
    }

    private void onNavigationSwitch(int position){
        viewPagerTab.setCurrentItem(position);
        clearSearch();
        CURRENT_MENU = position;
    }

    private int getPrevPosition(int currentPosition){
        bottomNavigation.getMenu().getItem(currentPosition).setChecked(true);
        CURRENT_MENU = currentPosition;
        clearSearch();

        return currentPosition;
    }

    @Override
    public void onMovieItemClick(Object movie, HashMap<String, View> viewHashMap) {
        if (!isClickEnabled()) return;

        setClickEnabled(false);
        if (movie instanceof TvMovie) showTvMovieDetail((TvMovie) movie, viewHashMap);
        else if (movie instanceof Movie) showMovieDetail((Movie) movie, viewHashMap);

        if (!movieSearchView.getQuery().toString().isEmpty()) {
            clearSearch();
        }
    }

    @Override
    public void onRefreshClick() {
        hideRefresh();
        showLoadingSlide(true);
        movieViewModel.getNowPlayingMovieListFromRemote(PAGE);
    }

    @Override
    public void onFavoriteClick() {
        Intent intent = new Intent(this, FavoriteMovieActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_movie:
                onNavigationSwitch(POSITION_MOVIE);
                return true;
            case R.id.action_tv_movie:
                onNavigationSwitch(POSITION_TVMOVIE);
                return true;
        }
        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        bottomNavigation.getMenu().getItem(prevMenuPosition).setChecked(false);
        if (prevMenuPosition == POSITION_MOVIE) position = getPrevPosition(POSITION_TVMOVIE);
        else if (prevMenuPosition == POSITION_TVMOVIE) position = getPrevPosition(POSITION_MOVIE);

        prevMenuPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        clearSearch();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (CURRENT_MENU == POSITION_MOVIE) movieViewModel.getSearchMovieListFromRemote(newText);
        else movieViewModel.getSearchTvMovieListFromRemote(newText);

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        movieViewModel.dispose(this);
    }
}
