package com.prasas.moviecatalogue.ui.dailynotification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.prasas.moviecatalogue.R;

import java.util.HashMap;

import static com.prasas.moviecatalogue.ui.dailynotification.AlarmNotificationReceiver.CHANNEL_RELEASE_TODAY_REMINDER;

public class NotifHelper {

    private static NotifHelper notifHelper;

    private NotifHelper(){

    }

    public static NotifHelper getInstance(){
        if (notifHelper == null){
            synchronized (NotifHelper.class){
                if (notifHelper == null){
                    notifHelper = new NotifHelper();
                }
            }
        }

        return notifHelper;
    }

    void nootifBuilder(Context context, String channelId, String title, String message, PendingIntent pendingIntent, String channelName, int notifId, HashMap<String, Bitmap> bitmapHashMap){
        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_movie_black)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);

        if (channelId.equalsIgnoreCase(CHANNEL_RELEASE_TODAY_REMINDER)){
            builder.setLargeIcon(bitmapHashMap.get("logo"))
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(bitmapHashMap.get("large"))
                            .bigLargeIcon(null));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_DEFAULT);

            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(channelId);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        Notification notification = builder.build();

        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, notification);
        }
    }
}
