package com.prasas.moviecatalogue.ui.dailynotification;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.repository.RemoteRepository;
import com.prasas.moviecatalogue.ui.splash.SplashScreenActivity;
import com.prasas.moviecatalogue.utils.OnGetMovieCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.prasas.moviecatalogue.utils.Constant.TYPE_DAILY_REMINDER;
import static com.prasas.moviecatalogue.utils.Constant.TYPE_RELEASE_TODAY_REMINDER;

public class AlarmNotificationReceiver extends BroadcastReceiver {
    public static final int ID_DAILY_REMINDER = 100;
    public static final int ID_RELEASE_TODAY_REMINDER = 101;
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_TYPE = "type";
    public static final String CHANNEL_DAILY_REMINDER = "Channel_Daily_Reminder";
    public static final String CHANNEL_RELEASE_TODAY_REMINDER = "Channel_Today_Release_Reminder";

    public AlarmNotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String type = intent.getStringExtra(EXTRA_TYPE);
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        if (type != null && message != null) {
            String title = type.equalsIgnoreCase(TYPE_DAILY_REMINDER) ? context.getString(R.string.daily_reminder) : context.getString(R.string.release_today_reminder);
            int notifId = type.equalsIgnoreCase(TYPE_DAILY_REMINDER) ? ID_DAILY_REMINDER : ID_RELEASE_TODAY_REMINDER;
            String channelId = type.equalsIgnoreCase(TYPE_DAILY_REMINDER) ? CHANNEL_DAILY_REMINDER : CHANNEL_RELEASE_TODAY_REMINDER;

            if (type.equalsIgnoreCase(TYPE_DAILY_REMINDER)) {
                showAlarmNotification(context, title, message, channelId, notifId, null);
            } else {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                String date = format.format(new Date());
                getReleaseTodayMovie(context, date, new OnGetMovieCallback<List<Movie>>() {
                    @Override
                    public void onSuccess(List<Movie> movieList) {
                        if (movieList.size() > 0) {
                            showAlarmNotification(context, title, movieList.get(0).getTitle(), channelId, notifId, movieList.get(0).getBackdrop_path());
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }
                });
            }
        }
    }

    public void setDailyReminderAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 7);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Intent intent = new Intent(context, AlarmNotificationReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, context.getString(R.string.daily_reminder_label));
        intent.putExtra(EXTRA_TYPE, TYPE_DAILY_REMINDER);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ID_DAILY_REMINDER, intent, 0);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    public void setReleaseTodayReminderAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Intent intent = new Intent(context, AlarmNotificationReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, context.getString(R.string.release_today_reminder_label));
        intent.putExtra(EXTRA_TYPE, TYPE_RELEASE_TODAY_REMINDER);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ID_RELEASE_TODAY_REMINDER, intent, 0);
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    private void showAlarmNotification(Context context, String title, String message, String channelId, int notifId, @Nullable String imageUrl) {
        String CHANNEL_NAME = "AlarmManager channel";

        Intent intent = new Intent(context, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        if (channelId.equalsIgnoreCase(CHANNEL_DAILY_REMINDER)) {
            NotifHelper.getInstance().nootifBuilder(context, channelId, title, message, pendingIntent, CHANNEL_NAME, notifId, null);
        } else {
            new getBitmapAsyncTask(pendingIntent, context, title, message, channelId, CHANNEL_NAME, notifId).execute(imageUrl);
        }
    }

    public void cancelAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmNotificationReceiver.class);
        int requestCode = type.equalsIgnoreCase(TYPE_DAILY_REMINDER) ? ID_DAILY_REMINDER : ID_RELEASE_TODAY_REMINDER;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        pendingIntent.cancel();
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    private void getReleaseTodayMovie(Context context, String date, OnGetMovieCallback<List<Movie>> callback) {
        RemoteRepository.getInstance()
                .setupReleaseMovieListObservable(context.getApplicationContext(), new OnGetMovieCallback<List<Movie>>() {
                    @Override
                    public void onSuccess(List<Movie> movieList) {
                        callback.onSuccess(movieList);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        callback.onError(throwable);
                    }
                });

        new Handler().postDelayed(()
                -> RemoteRepository.getInstance().fetchReleaseMovieListFromRemote(date), 1000);
    }

    private static class getBitmapAsyncTask extends AsyncTask<String, Void, HashMap<String, Bitmap>> {
        private PendingIntent pendingIntent;
        @SuppressLint("StaticFieldLeak")
        private Context context;
        private String title;
        private String message;
        private String channelId;
        private String channelName;
        private int notifId;

        getBitmapAsyncTask(PendingIntent pendingIntent, Context context, String title, String message, String channelId, String channelName, int notifId) {
            this.pendingIntent = pendingIntent;
            this.context = context.getApplicationContext();
            this.title = title;
            this.message = message;
            this.channelId = channelId;
            this.channelName = channelName;
            this.notifId = notifId;
        }

        private Bitmap getBitmap(String imageUrl) throws Exception {
            return Glide.with(context.getApplicationContext())
                    .asBitmap()
                    .load(imageUrl)
                    .error(R.drawable.ic_image)
                    .submit()
                    .get();
        }

        @Override
        protected HashMap<String, Bitmap> doInBackground(String... strings) {
            try {
                HashMap<String, Bitmap> bitmapHashMap = new HashMap<>();
                Bitmap bitmapLogo = getBitmap(BuildConfig.PHOTO_BASE_URL_LOGO + strings[0]);
                bitmapHashMap.put("logo", bitmapLogo);

                Bitmap bitmapLarge = getBitmap(BuildConfig.PHOTO_BASE_URL_MEDIUM + strings[0]);
                bitmapHashMap.put("large", bitmapLarge);

                return bitmapHashMap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(HashMap<String, Bitmap> stringBitmapHashMap) {
            super.onPostExecute(stringBitmapHashMap);

            if (stringBitmapHashMap == null) {
                return;
            }

            NotifHelper.getInstance().nootifBuilder(context, channelId, title, message, pendingIntent, channelName, notifId, stringBitmapHashMap);
        }
    }
}
