package com.prasas.moviecatalogue.ui.favoritemovie;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.prasas.moviecatalogue.core.BaseActivity;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.PagerListAdapter;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import butterknife.BindView;

public class FavoriteMovieActivity extends BaseActivity<MovieViewModel> {

    @BindView(R.id.tabs_favorite)
    TabLayout tabsFavorite;
    @BindView(R.id.view_pager_list)
    ViewPager viewPagerList;

    @Override
    protected void onCreate(Bundle instance, MovieViewModel viewModel) {
        initView();
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    public void initView() {
        setTitle(getString(R.string.favorite));

        initTabs();
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_favorite;
    }

    @Override
    protected int getToolbarType() {
        return Constant.TOOLBAR_TYPE_DETAIL;
    }

    private void initTabs() {
        PagerListAdapter pagerAdapter;
        pagerAdapter = new PagerListAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new FavoriteMovieListFragment(), getString(R.string.movies));
        pagerAdapter.addFragment(new FavoriteTvMovieListFragment(), getString(R.string.tv_movies));
        viewPagerList.setAdapter(pagerAdapter);
        tabsFavorite.setupWithViewPager(viewPagerList);
    }
}
