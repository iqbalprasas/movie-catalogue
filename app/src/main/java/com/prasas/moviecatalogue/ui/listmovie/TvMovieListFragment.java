package com.prasas.moviecatalogue.ui.listmovie;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseFragment;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.ui.detailmovie.MovieDetailActivity;
import com.prasas.moviecatalogue.utils.MovieLayoutManager;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.utils.PaginationScrollListener;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

public class TvMovieListFragment extends BaseFragment<MovieViewModel> implements OnClickListener.ListTvMovieClick {

    @BindView(R.id.recycler_movies)
    RecyclerView recyclerNews;
    @BindView(R.id.swipe_movies)
    SwipeRefreshLayout swipeMovies;
    @BindView(R.id.tx_message)
    TextView txMessage;
    @BindView(R.id.btn_refresh)
    ImageButton btnRefresh;

    private MovieViewModel movieViewModel;
    private List<TvMovie> idFavoriteList;
    private List<TvMovie> tvMovieList;
    private TvMovieListAdapter tvMovieListAdapter;
    private MovieLayoutManager layoutManager;
    private boolean isLoadingMoreTvMovie = false;
    private boolean isLastPage = false;
    private static int PAGE = 1;

    public TvMovieListFragment() {
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movies;
    }

    @Override
    protected void onActivityCreated(Bundle instance, MovieViewModel viewModel) {
        initViewModel(viewModel);

        if (instance == null) {
            movieViewModel.getTvMovieListFromRemote(PAGE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        idFavoriteList = new ArrayList<>();
        tvMovieList = new ArrayList<>();

        initView();

        showLoading(true);
    }

    @Override
    public void initView() {
        initRecycler();
        btnRefresh.setOnClickListener((View v) -> this.onRefreshClick());
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
        movieViewModel.getTvMovieListLiveData().observe(getViewLifecycleOwner(), tvMovies -> {
            if (isShowMessage()) {
                hideMessage();
            }
            tvMovieList = tvMovies;
            syncAllTvMovieListWithFavorite();
        });
        movieViewModel.getMessageTvMovieList().observe(getViewLifecycleOwner(), message -> {
            showLoading(false);
            if (message.isError()) {
                showMessage(message.getMessage());
            }
        });
        movieViewModel.getFavoriteTvMovieList().observe(getViewLifecycleOwner(), tvMovies -> {
            idFavoriteList = tvMovies;
            if (tvMovieList.size() != 0) {
                syncAllTvMovieListWithFavorite();
            }
            if (!isClickEnabled()) {
                setClickEnabled(true);
                showLoading(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setClickEnabled(true);
    }

    private void initRecycler() {
        recyclerNews.setHasFixedSize(true);
        tvMovieListAdapter = new TvMovieListAdapter(this);
        layoutManager = new MovieLayoutManager(getContext(), 2);
        layoutManager.setSpanSizeLookup(new MovieLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // show loading in a row
                // show movie in 2 row
                if (tvMovieListAdapter.getItemViewType(position) == MovieListAdapter.VIEW_TYPE_LOADING) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        recyclerNews.setLayoutManager(layoutManager);
        recyclerNews.setItemAnimator(new DefaultItemAnimator());
        recyclerNews.setAdapter(tvMovieListAdapter);
        recyclerNews.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                recyclerNews.post(() -> tvMovieListAdapter.notifyItemInserted(tvMovieListAdapter.getItemCount()));
                showMoreMovies();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoadingMoreTvMovie;
            }
        });
        swipeMovies.setOnRefreshListener(() -> {
            isLoadingMoreTvMovie = false;
            isLastPage = false;
            swipeMovies.setRefreshing(false);
        });
    }

    private void showTvMovieDetail(TvMovie tvMovie) {
        // goto tvmovie detail
        Intent intent = new Intent(getContext(), MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.TVMOVIE_DETAIL, tvMovie);

        startActivity(intent);
    }

    private void showMoreMovies() {
        // add dummy loading
        tvMovieListAdapter.addLoading();
        isLoadingMoreTvMovie = true;
        // show loading in 3 seconds
        new Handler().postDelayed(() -> {
            isLoadingMoreTvMovie = false;
            tvMovieListAdapter.removeLoading();
        }, 3000);
    }

    private void syncAllTvMovieListWithFavorite() {
        int i = 0;
        for (TvMovie m : tvMovieList) {
            tvMovieList.get(i).setFavorite(false);
            for (TvMovie ftm : idFavoriteList) {
                if (m.equalsId(ftm)) {
                    tvMovieList.get(i).setFavorite(true);
                }
            }
            i++;
        }
        tvMovieListAdapter.setTvMovieList(tvMovieList);
    }

    private void showMessage(String message) {
        swipeMovies.setVisibility(View.GONE);
        txMessage.setVisibility(View.VISIBLE);
        btnRefresh.setVisibility(View.VISIBLE);
        txMessage.setText(message);
    }

    private void hideMessage() {
        swipeMovies.setVisibility(View.VISIBLE);
        txMessage.setVisibility(View.INVISIBLE);
        btnRefresh.setVisibility(View.INVISIBLE);
    }

    private boolean isShowMessage() {
        return (txMessage.getVisibility() == View.VISIBLE);
    }

    @Override
    public void onTvMovieItemClick(TvMovie tvMovie) {
        if (isClickEnabled()) {
            setClickEnabled(false);
            showTvMovieDetail(tvMovie);
        }
    }

    @Override
    public void onFavoriteItemClick(TvMovie tvMovie, boolean isFavorite, int position) {
        if (!isClickEnabled()) {
            return;
        }
        setClickEnabled(false);
        showLoading(true);
        if (isFavorite) {
            movieViewModel.removeFavoriteTvMovie(tvMovie);
            Toast.makeText(getContext(), Objects.requireNonNull(getContext()).getString(R.string.removed_from_favorite, tvMovie.getName()), Toast.LENGTH_SHORT).show();
        } else {
            movieViewModel.addFavoriteTvMovie(tvMovie);
            Toast.makeText(getContext(), Objects.requireNonNull(getContext()).getString(R.string.added_to_favorite, tvMovie.getName()), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefreshClick() {
        hideMessage();
        showLoading(true);
        movieViewModel.getTvMovieListFromRemote(PAGE);
    }
}
