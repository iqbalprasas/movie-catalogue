package com.prasas.moviecatalogue.ui.detailmovie;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseActivity;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.utils.PreferenceManager;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import butterknife.BindView;


public class MovieDetailActivity extends BaseActivity<MovieViewModel> implements OnClickListener.MovieDetailClick {

    @BindView(R.id.collapsing_movie_detail)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.appbar_movie_detail)
    AppBarLayout appBarLayout;
    @BindView(R.id.img_backdrop)
    ImageView imgBackDrop;
    @BindView(R.id.tx_title)
    TextView txTitle;
    @BindView(R.id.tx_rating)
    TextView txRating;
    @BindView(R.id.tx_voters)
    TextView txVoters;
    @BindView(R.id.tx_pg)
    TextView txPg;
    @BindView(R.id.tx_release_date)
    TextView txReleaseDate;
    @BindView(R.id.tx_genre)
    TextView txGenre;
    @BindView(R.id.tx_synopsis)
    TextView txSynopsis;
    @BindView(R.id.fab_favorite_active)
    FloatingActionButton fabFavoriteActive;
    @BindView(R.id.progressbar_layout)
    FrameLayout progressBar;

    private MovieViewModel movieViewModel;

    private Movie movie;
    private String title = "";
    private boolean isMovie;

    public final static String MOVIE_DETAIL = "MovieDetailActivity";
    public final static String TVMOVIE_DETAIL = "TvMovieDetail";

    @Override
    protected void onCreate(Bundle instance, MovieViewModel viewModel) {
        if (!(getIntent().hasExtra(MOVIE_DETAIL) || getIntent().hasExtra(TVMOVIE_DETAIL))) {
            showAlertError(getString(R.string.error_message_general));
            return;
        }

        initViewModel(viewModel);

        initView();
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_movie_detail;
    }

    @Override
    protected int getToolbarType() {
        return Constant.TOOLBAR_TYPE_COLLAPSING_AND_DETAIL;
    }

    @Override
    public void initView() {
        initToolbar();

        loadImage(movie.getBackdrop_path());
        txTitle.setText(title);
        txRating.setText(String.valueOf(movie.getVoteAverage()));
        txVoters.setText(getString(R.string.vote, movie.getVoteCount()));
        txPg.setText(movie.isAdult() ? "R" : "G");
        txReleaseDate.setText(isMovie
                ? movie.getReleaseDate()
                : ((TvMovie) movie).getFirstAirDate());
        try {
            txGenre.setText(PreferenceManager.getGenreByMovie(movie));
        } catch (Exception e) {
            txGenre.setText("");
        }
        txSynopsis.setText(movie.getOverview());

        fabFavoriteActive.setOnClickListener((View view) -> this.onFavoriteClick(false));
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {
        movieViewModel = viewModel;
        isMovie = getIntent().hasExtra(MOVIE_DETAIL);
        if (isMovie) {
            movie = getIntent().getParcelableExtra(MovieDetailActivity.MOVIE_DETAIL);
            title = movie != null ? movie.getTitle() : "";
            movieViewModel.getFavoriteMovieList().observe(this, movies -> {
                for (Movie m : movies) {
                    if (m.getId() == movie.getId()) showFavoriteButton(true);
                }
                showLoading(false);
                setClickEnabled(true);
            });
        } else {
            movie = getIntent().getParcelableExtra(MovieDetailActivity.TVMOVIE_DETAIL);
            title = movie != null ? ((TvMovie) movie).getName() : "";
            movieViewModel.getFavoriteTvMovieList().observe(this, tvMovies -> {
                for (TvMovie m : tvMovies) {
                    if (m.getId() == movie.getId()) showFavoriteButton(true);
                }
                showLoading(false);
                setClickEnabled(true);
            });
        }
    }

    private void initToolbar() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + i == 0) {
                    collapsingToolbarLayout.setTitle(title);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
                txTitle.setAlpha(((float) (scrollRange + i) / scrollRange < 0.3) && !isShow
                        ? (float) 0.3
                        : (float) (scrollRange + i) / scrollRange);
            }
        });
    }

    private void loadImage(String path) {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setColorSchemeColors(Color.WHITE);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();

        Glide.with(this)
                .load(BuildConfig.PHOTO_BASE_URL_MEDIUM + path)
                .centerCrop()
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_image)
                .into(imgBackDrop);
    }

    public void showAlertError(String message) {
        Toast.makeText(MovieDetailActivity.this, message, Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    private void showFavoriteButton(boolean isFavorite) {
        if (isFavorite) {
            fabFavoriteActive.setImageDrawable(getResources().getDrawable(R.drawable.ic_love_red_big, getTheme()));
            movie.setFavorite(true);
        } else {
            fabFavoriteActive.setImageDrawable(getResources().getDrawable(R.drawable.ic_love_gray_big, getTheme()));
            movie.setFavorite(false);
        }
    }

    private void showLoading(boolean isShow) {
        progressBar.setVisibility(isShow
                ? View.VISIBLE
                : View.GONE);
    }

    @Override
    public void onFavoriteClick(boolean isFavorite) {
        if (!isClickEnabled()) return;

        showLoading(true);
        setClickEnabled(false);
        if (!movie.isFavorite()) {
            if (isMovie) movieViewModel.addFavoriteMovie(movie);
            else movieViewModel.addFavoriteTvMovie((TvMovie) movie);
            Toast.makeText(this, getString(R.string.added_to_favorite, title), Toast.LENGTH_SHORT).show();
        } else {
            if (isMovie) movieViewModel.removeFavoriteMovie(movie);
            else movieViewModel.removeFavoriteTvMovie((TvMovie) movie);
            Toast.makeText(this, getString(R.string.removed_from_favorite, title), Toast.LENGTH_SHORT).show();
        }
        showFavoriteButton(isFavorite);
    }
}
