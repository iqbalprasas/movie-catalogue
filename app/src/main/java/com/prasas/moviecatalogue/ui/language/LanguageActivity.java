package com.prasas.moviecatalogue.ui.language;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseActivity;
import com.prasas.moviecatalogue.utils.Constant;
import com.prasas.moviecatalogue.utils.OnClickListener;
import com.prasas.moviecatalogue.utils.PreferenceManager;
import com.prasas.moviecatalogue.viewmodel.MovieViewModel;

import butterknife.BindView;

import static com.prasas.moviecatalogue.utils.Constant.IS_LANGUAGE_CHANGED;
import static com.prasas.moviecatalogue.utils.Constant.LANGUAGE_KEY;

public class LanguageActivity extends BaseActivity<MovieViewModel> implements OnClickListener.LanguageClick {
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.radio_group_language)
    RadioGroup radioLanguage;
    @BindView(R.id.radio_en)
    RadioButton radioEn;
    @BindView(R.id.radio_in)
    RadioButton radioIn;

    @Override
    protected void onCreate(Bundle instance, MovieViewModel viewModel) {
        initView();

        setSavedLanguage();
    }

    @Override
    protected Class<MovieViewModel> getViewModel() {
        return MovieViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_language_setting;
    }

    @Override
    protected int getToolbarType() {
        return Constant.TOOLBAR_TYPE_DETAIL;
    }

    @Override
    public void initView() {
        setTitle(getString(R.string.language));

        btnSave.setOnClickListener((View view) -> this.onSaveClick());
        radioLanguage.setOnCheckedChangeListener((view, rg) -> this.onRadioClick(view.getCheckedRadioButtonId()));
    }

    @Override
    protected void initViewModel(MovieViewModel viewModel) {

    }

    public void setSavedLanguage() {
        if (tinyDB.getString(LANGUAGE_KEY) != null) {
            if (tinyDB.getString(LANGUAGE_KEY).equals(PreferenceManager.LANGUAGE_ENGLISH)) {
                radioEn.setChecked(true);
                radioIn.setChecked(false);
            } else {
                radioEn.setChecked(false);
                radioIn.setChecked(true);
            }
        }
    }

    public void setLanguage() {
        if (radioEn.isChecked()) {
            tinyDB.putString(LANGUAGE_KEY, PreferenceManager.LANGUAGE_ENGLISH);
        } else {
            tinyDB.putString(LANGUAGE_KEY, PreferenceManager.LANGUAGE_INDONESIA);
        }
        tinyDB.putBoolean(IS_LANGUAGE_CHANGED, true);
        PreferenceManager.setLocale(LanguageActivity.this);
        recreate();
    }

    @Override
    public void onSaveClick() {
        if (!isClickEnabled()) return;

        setLanguage();
        setClickEnabled(false);
    }

    @Override
    public void onRadioClick(int checkedRadio) {
        if (checkedRadio == R.id.radio_en) {
            if (tinyDB.getString(LANGUAGE_KEY).equals(PreferenceManager.LANGUAGE_ENGLISH)) {
                setClickEnabled(false);
                return;
            }
        } else if (checkedRadio == R.id.radio_in) {
            if (tinyDB.getString(LANGUAGE_KEY).equals(PreferenceManager.LANGUAGE_INDONESIA)) {
                setClickEnabled(false);
                return;
            }
        }
        setClickEnabled(true);
    }
}
