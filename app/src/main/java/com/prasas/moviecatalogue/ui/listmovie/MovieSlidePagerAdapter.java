package com.prasas.moviecatalogue.ui.listmovie;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.utils.OnClickListener;

import java.util.HashMap;
import java.util.List;

public class MovieSlidePagerAdapter extends PagerAdapter {
    private Context context;
    private OnClickListener.MovieClick onItemClickListener;
    private List<Movie> contentSlide;
    private CircularProgressDrawable circularProgressDrawable;

    public void setOnClickListener(OnClickListener.MovieClick onClickListener) {
        this.onItemClickListener = onClickListener;
    }

    MovieSlidePagerAdapter(Context context) {
        this.context = context;
        circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setColorSchemeColors(Color.WHITE);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
    }

    void setContentSlide(List<Movie> movieList) {
        this.contentSlide = movieList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contentSlide.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_slide, null);
        ConstraintLayout layout = view.findViewById(R.id.layout_item_slide);
        ImageView imgTitle = view.findViewById(R.id.img_slide);
        TextView txTitle = view.findViewById(R.id.tx_slide);

        Glide.with(context)
                .load(BuildConfig.PHOTO_BASE_URL_MEDIUM + contentSlide.get(position).getBackdrop_path())
                .centerCrop()
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_image)
                .into(imgTitle);
        txTitle.setText(contentSlide.get(position).getTitle());
        if (onItemClickListener != null) {
            HashMap<String, View> viewHashMap = new HashMap<>();
            viewHashMap.put("titleMovie", txTitle);
            layout.setOnClickListener((View v) -> onItemClickListener.onMovieItemClick(contentSlide.get(position), viewHashMap));
        }

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }
}
