package com.prasas.moviecatalogue.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.prasas.moviecatalogue.dao.MovieDao;
import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;

@Database(entities = {Movie.class, TvMovie.class, GenreByMovie.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase appDatabase;

    public static AppDatabase getInstance(Context context) {
        if (appDatabase == null) {
            synchronized (AppDatabase.class){
                if (appDatabase == null){
                    String TAG = "AppDatabase";
                    appDatabase = Room
                            .databaseBuilder(context.getApplicationContext(), AppDatabase.class, TAG)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return appDatabase;
    }

    public abstract MovieDao getMovieDao();

}
