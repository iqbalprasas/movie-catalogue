package com.prasas.moviecatalogue.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.prasas.moviecatalogue.core.BaseViewModel;
import com.prasas.moviecatalogue.data.entity.Genre;
import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.Message;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.data.repository.MovieRepository;
import com.prasas.moviecatalogue.utils.OnGetGenreCallback;

import java.util.List;

public class MovieViewModel extends BaseViewModel {

    private LiveData<List<Movie>> nowPlayingMovieListLiveData;
    private LiveData<List<Movie>> movieListLiveData;
    private LiveData<List<TvMovie>> tvMovieListLiveData;
    private LiveData<List<Movie>> favoriteMovieList;
    private LiveData<List<TvMovie>> favoriteTvMovieList;
    private LiveData<List<Movie>> searchMovieListLiveData;
    private LiveData<List<TvMovie>> searchTvMovieListLiveData;
    private LiveData<Message> messageNowPlayingMovieList;
    private LiveData<Message> messageMovieList;
    private LiveData<Message> messageTvMovieList;
    private LiveData<List<GenreByMovie>> genreByMovieList;
    private LiveData<List<GenreByMovie>> favoriteMovieWithGenreList;
    private LiveData<List<GenreByMovie>> favoriteTvMovieWithGenreList;
    private MovieRepository movieRepository;

    public MovieViewModel(@NonNull Application application) {
        super(application);
        movieRepository = MovieRepository.getInstance(application);
        nowPlayingMovieListLiveData = movieRepository.getNowPlayingMovieListLiveData();
        movieListLiveData = movieRepository.getMovieListLiveData();
        tvMovieListLiveData = movieRepository.getTvMovieListLiveData();
        favoriteMovieList = movieRepository.getFavoriteMovieList();
        favoriteTvMovieList = movieRepository.getFavoriteTvMovieList();
        searchMovieListLiveData = movieRepository.getSearchMovieListLiveData();
        searchTvMovieListLiveData = movieRepository.getSearchTvMovieListLiveData();
        messageNowPlayingMovieList = movieRepository.getMessageNowPlayingMovieList();
        messageMovieList = movieRepository.getMessageMovieList();
        messageTvMovieList = movieRepository.getMessageTvMovieList();
        genreByMovieList = movieRepository.getGenreMovieList();
        favoriteMovieWithGenreList = Transformations.switchMap(favoriteMovieList, input -> getGenreByMovieList());
        favoriteTvMovieWithGenreList = Transformations.switchMap(favoriteTvMovieList, input -> getGenreByMovieList());
    }

    public void setupMovieListObservable(Context context){
        movieRepository.setupMovieListObservable(context);
    }

    public void setupNowPlayingMovieListObservable(Context context){
        movieRepository.setupNowPlayingMovieListObservable(context);
    }

    public void setupTvMovieListObservable(Context context){
        movieRepository.setupTvMovieListObservable(context);
    }

    public void setupSearchMovieListObservable(Context context){
        movieRepository.setupSearchMovieListObservable(context);
    }

    public void setupSearchTvMovieListObservable(Context context){
        movieRepository.setupSearchTvMovieListObservable(context);
    }

    public LiveData<List<GenreByMovie>> getFavoriteMovieWithGenreList() {
        return favoriteMovieWithGenreList;
    }

    public LiveData<List<GenreByMovie>> getFavoriteTvMovieWithGenreList() {
        return favoriteTvMovieWithGenreList;
    }

    public LiveData<List<Movie>> getNowPlayingMovieListLiveData() {
        return nowPlayingMovieListLiveData;
    }

    public LiveData<List<Movie>> getMovieListLiveData() {
        return movieListLiveData;
    }

    public LiveData<List<TvMovie>> getTvMovieListLiveData() {
        return tvMovieListLiveData;
    }

    public LiveData<List<Movie>> getFavoriteMovieList() {
        return favoriteMovieList;
    }

    public LiveData<List<Movie>> getSearchMovieListLiveData() {
        return searchMovieListLiveData;
    }

    public LiveData<List<TvMovie>> getSearchTvMovieListLiveData() {
        return searchTvMovieListLiveData;
    }

    private LiveData<List<GenreByMovie>> getGenreByMovieList() {
        return genreByMovieList;
    }

    public LiveData<List<TvMovie>> getFavoriteTvMovieList() {
        return favoriteTvMovieList;
    }

    public LiveData<Message> getMessageNowPlayingMovieList() {
        return messageNowPlayingMovieList;
    }

    public LiveData<Message> getMessageMovieList() {
        return messageMovieList;
    }

    public LiveData<Message> getMessageTvMovieList() {
        return messageTvMovieList;
    }

    public void getNowPlayingMovieListFromRemote(int page) {
        movieRepository.fetchNowPlayingMovieListFromRemote(page);
    }

    public void getMovieListFromRemote(int page) {
        movieRepository.fetchMovieListFromRemote(page);
    }

    public void getTvMovieListFromRemote(int page) {
        movieRepository.fetchTvMovieListFromRemote(page);
    }

    public void getSearchMovieListFromRemote(String query){
        movieRepository.fetchSearchMovieListFromRemote(query);
    }

    public void getSearchTvMovieListFromRemote(String query){
        movieRepository.fetchSearchTvMovieListFromRemote(query);
    }

    public void addFavoriteMovie(Movie movie) {
        movieRepository.addFavoriteMovie(movie);
    }

    public void addFavoriteTvMovie(TvMovie tvMovie) {
        movieRepository.addFavoriteTvMovie(tvMovie);
    }

    public void removeFavoriteMovie(Movie movie) {
        movieRepository.removeFavoriteMovie(movie);
    }

    public void removeFavoriteTvMovie(TvMovie tvMovie) {
        movieRepository.removeFavoriteTvMovie(tvMovie);
    }

    public Movie getMovie(int id){
        return movieRepository.getMovie(id);
    }

    public void getGenreList(Context context, OnGetGenreCallback callback){
        movieRepository.getGenreList(context, new OnGetGenreCallback() {
            @Override
            public void onSuccess(List<Genre> genreList) {
                callback.onSuccess(genreList);
            }

            @Override
            public void onError(Throwable throwable) {
                callback.onError(throwable);
            }
        });
    }

    @Override
    public MovieRepository getMovieRepository() {
        return movieRepository;
    }
}
