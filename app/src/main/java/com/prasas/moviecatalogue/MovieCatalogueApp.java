package com.prasas.moviecatalogue;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.NonNull;

import com.prasas.moviecatalogue.utils.PreferenceManager;

public class MovieCatalogueApp extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(PreferenceManager.setLocale(base));
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        PreferenceManager.setLocale(this);
    }
}
