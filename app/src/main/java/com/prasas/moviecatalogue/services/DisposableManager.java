package com.prasas.moviecatalogue.services;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class DisposableManager {

    private static volatile DisposableManager disposableManagerInstance;
    private Map<Object, CompositeDisposable> compositeDisposableMap = new HashMap<>();

    private DisposableManager() {
        if (disposableManagerInstance != null) {
            throw new RuntimeException("NetworEvent should only have single instance. please use getInstance()");
        }
    }

    public static DisposableManager getInstance() {
        if (disposableManagerInstance == null) {
            synchronized (DisposableManager.class) {
                if (disposableManagerInstance == null) {
                    disposableManagerInstance = new DisposableManager();
                }
            }
        }
        return disposableManagerInstance;
    }

    @NonNull
    private CompositeDisposable getCompositeDisposable(@NonNull Object object) {
        CompositeDisposable compositeDisposable = compositeDisposableMap.get(object);
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
            compositeDisposableMap.put(object, compositeDisposable);
        }
        return compositeDisposable;
    }

    public void unregister(@NonNull Object lifecycle) {
        CompositeDisposable compositeDisposable = compositeDisposableMap.remove(lifecycle);
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }

    public void addDisposable(@NonNull Object lifecycle, @NonNull Disposable disposable) {
        getCompositeDisposable(lifecycle).add(disposable);
    }
}
