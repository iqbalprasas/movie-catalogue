package com.prasas.moviecatalogue.services;

import com.prasas.moviecatalogue.data.entity.BaseResponse;
import com.prasas.moviecatalogue.data.entity.Genre;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("movie/popular")
    Observable<Response<BaseResponse<ArrayList<Movie>>>> getPopularMovies(@Query("api_key") String apiKey,
                                                                          @Query("page") int page);

    @GET("tv/popular")
    Observable<Response<BaseResponse<ArrayList<TvMovie>>>> getPopularTvMovies(@Query("api_key") String apiKey,
                                                                              @Query("page") int page);

    @GET("movie/now_playing")
    Observable<Response<BaseResponse<ArrayList<Movie>>>> getNowPlayingMovies(@Query("api_key") String apiKey,
                                                                             @Query("page") int page);

    @GET("genre/movie/list")
    Observable<Response<ArrayList<Genre>>> getGenres(@Query("api_key") String apiKey);

    @GET("search/movie")
    Observable<Response<BaseResponse<ArrayList<Movie>>>> getSearchMovies(@Query("api_key") String apiKey,
                                                                         @Query("query") String query);

    @GET("search/tv")
    Observable<Response<BaseResponse<ArrayList<TvMovie>>>> getSearchTvMovies(@Query("api_key") String apiKey,
                                                                             @Query("query") String query);

    @GET("discover/movie")
    Observable<Response<BaseResponse<ArrayList<Movie>>>> getReleaseMovies(@Query("api_key") String apiKey,
                                                                          @Query("primary_release_date.gte") String dateGte,
                                                                          @Query("primary_release_date.lte") String dateLte);
}
