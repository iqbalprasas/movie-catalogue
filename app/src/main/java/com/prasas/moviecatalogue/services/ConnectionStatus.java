package com.prasas.moviecatalogue.services;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionStatus extends ContextWrapper {

    public ConnectionStatus(Context base) {
        super(base);
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo connection = manager != null ? manager.getActiveNetworkInfo() : null;
        return connection != null && connection.isConnectedOrConnecting();
    }
}
