package com.prasas.moviecatalogue.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {
    @SerializedName("results")
    @Expose
    private T results;

    public T getResults() {
        return results;
    }
}
