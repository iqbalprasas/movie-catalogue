package com.prasas.moviecatalogue.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class TvMovie extends Movie implements Parcelable {
    @SerializedName("first_air_date")
    @Expose
    private String firstAirDate;
    @SerializedName("name")
    @Expose
    private String name;

    public TvMovie() {
    }

    protected TvMovie(Parcel in) {
        super(in);
        firstAirDate = in.readString();
        name = in.readString();
    }

    public static final Creator<TvMovie> CREATOR = new Creator<TvMovie>() {
        @Override
        public TvMovie createFromParcel(Parcel source) {
            return new TvMovie(source);
        }

        @Override
        public TvMovie[] newArray(int size) {
            return new TvMovie[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(firstAirDate);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public void setFirstAirDate(String firstAirDate) {
        this.firstAirDate = firstAirDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
