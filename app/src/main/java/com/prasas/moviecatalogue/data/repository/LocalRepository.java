package com.prasas.moviecatalogue.data.repository;

import android.os.AsyncTask;

import com.prasas.moviecatalogue.dao.MovieDao;
import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.database.AppDatabase;

import java.util.ArrayList;
import java.util.List;

public class LocalRepository {

    private static volatile LocalRepository localRepository;

    private MovieDao movieDao;
    private AppDatabase appDatabase;

    private LocalRepository(MovieDao movieDao, AppDatabase appDatabase) {
        this.movieDao = movieDao;
        this.appDatabase = appDatabase;
    }

    public static LocalRepository getInstance(MovieDao movieDao, AppDatabase appDatabase) {
        if (localRepository == null) {
            synchronized (LocalRepository.class) {
                if (localRepository == null) {
                    localRepository = new LocalRepository(movieDao, appDatabase);
                }
            }
        }
        return localRepository;
    }

    private List<GenreByMovie> getAllGenreByMovie() {
        return movieDao.getAllGenreByMovieAsList().blockingFirst();
    }

    public List<Movie> getAllFavoriteMovie() {
        final List<Movie> movieList = movieDao.getAllMovieAsList().blockingFirst();
        final List<GenreByMovie> genreByMovieList = getAllGenreByMovie();

        int i = 0;
        for (Movie m : movieList) {
            movieList.get(i).setGenre_ids(new ArrayList<>());
            for (GenreByMovie gbm : genreByMovieList) {
                if (gbm.getIdMovie() == m.getId()) {
                    movieList.get(i).getGenre_ids().add(gbm.getIdGenre());
                }
            }
            i++;
        }

        return movieList;
    }

    public List<TvMovie> getAllFavoriteTvMovie() {
        final List<TvMovie> tvMovieList = movieDao.getAllTvMovieAsList().blockingFirst();
        final List<GenreByMovie> genreByMovieList = getAllGenreByMovie();

        int i = 0;
        for (TvMovie m : tvMovieList) {
            tvMovieList.get(i).setGenre_ids(new ArrayList<>());
            for (GenreByMovie gbm : genreByMovieList) {
                if (gbm.getIdMovie() == m.getId()) {
                    tvMovieList.get(i).getGenre_ids().add(gbm.getIdGenre());
                }
            }
            i++;
        }

        return tvMovieList;
    }

    Movie getMovie(int id) {
        return movieDao.getMovieAsObject(id).blockingFirst();
    }

    public void addFavoriteMovie(Movie movie) {
        new LocalRepository.addMovieAsyncTask(appDatabase).execute(movie);
    }

    public void addFavoriteTvMovie(TvMovie tvMovie) {
        new LocalRepository.addTvMovieAsyncTask(appDatabase).execute(tvMovie);
    }

    public void removeFavoriteMovie(Movie movie) {
        new LocalRepository.removeMovieAsyncTask(appDatabase).execute(movie);
    }

    public void removeFavoriteTvMovie(TvMovie tvMovie) {
        new LocalRepository.removeTvMovieAsyncTask(appDatabase).execute(tvMovie);
    }

    private static class addMovieAsyncTask extends AsyncTask<Movie, Void, Void> {
        private AppDatabase db;

        addMovieAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(Movie... movies) {
            List<GenreByMovie> genreByMovieList = new ArrayList<>();
            for (Integer gi : movies[0].getGenre_ids()) {
                genreByMovieList.add(new GenreByMovie(movies[0].getId(), gi));
            }
            db.getMovieDao().addMovieWithGenre(movies[0], genreByMovieList);
            return null;
        }
    }

    private static class addTvMovieAsyncTask extends AsyncTask<TvMovie, Void, Void> {
        private AppDatabase db;

        addTvMovieAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(TvMovie... tvMovies) {
            List<GenreByMovie> genreByMovieList = new ArrayList<>();
            for (Integer gi : tvMovies[0].getGenre_ids()) {
                genreByMovieList.add(new GenreByMovie(tvMovies[0].getId(), gi));
            }
            db.getMovieDao().addTvMovieWithGenre(tvMovies[0], genreByMovieList);
            return null;
        }
    }

    private static class removeMovieAsyncTask extends AsyncTask<Movie, Void, Void> {
        private AppDatabase db;

        removeMovieAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(Movie... movies) {
            db.getMovieDao().deleteMovieWithGenre(movies[0]);
            return null;
        }
    }

    private static class removeTvMovieAsyncTask extends AsyncTask<TvMovie, Void, Void> {
        private AppDatabase db;

        removeTvMovieAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(TvMovie... tvMovies) {
            db.getMovieDao().deleteTvMovieWithGenre(tvMovies[0]);
            return null;
        }
    }
}
