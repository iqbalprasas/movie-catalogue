package com.prasas.moviecatalogue.data.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class GenreByMovie {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int idMovie;
    private int idGenre;

    public GenreByMovie(int idMovie, int idGenre) {
        this.idMovie = idMovie;
        this.idGenre = idGenre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMovie() {
        return idMovie;
    }

    public int getIdGenre() {
        return idGenre;
    }
}
