package com.prasas.moviecatalogue.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.prasas.moviecatalogue.utils.Constant;

import java.util.ArrayList;

@Entity
public class Movie implements Parcelable {

    @SerializedName("vote_count")
    @Expose
    @ColumnInfo(name = "vote_count")
    private int voteCount;

    @PrimaryKey
    @SerializedName("id")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_ID)
    private int id;

    @SerializedName("video")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_VIDEO)
    private boolean video;

    @SerializedName("vote_average")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_VOTE_AVERAGE)
    private float voteAverage;

    @SerializedName("title")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_TITLE)
    private String title;

    @SerializedName("popularity")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_POPULARITY)
    private float popularity;

    @SerializedName("poster_path")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_POSTER_PATH)
    private String posterPath;

    @SerializedName("original_language")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_LANGUAGE)
    private String originalLanguage;

    @SerializedName("original_title")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_ORIGINAL_TITLE)
    private String originalTitle;

    @SerializedName("genre_ids")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_GENRE)
    @Ignore
    private ArrayList<Integer> genre_ids;

    @SerializedName("backdrop_path")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_BACKDROP_PATH)
    private String backdrop_path;

    @SerializedName("adult")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_ADULT)
    private boolean adult;

    @SerializedName("overview")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_OVERVIEW)
    private String overview;

    @SerializedName("release_date")
    @Expose
    @ColumnInfo(name = Constant.COLUMN_RELEASE_DATE)
    private String releaseDate;

    @Ignore
    private boolean isFavorite;

    public Movie() {
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public ArrayList<Integer> getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(ArrayList<Integer> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.voteCount);
        dest.writeInt(this.id);
        dest.writeInt(this.video ? 1 : 0);
        dest.writeFloat(this.voteAverage);
        dest.writeString(this.title);
        dest.writeFloat(this.popularity);
        dest.writeString(this.posterPath);
        dest.writeString(this.originalLanguage);
        dest.writeString(this.originalTitle);
        dest.writeList(this.genre_ids);
        dest.writeString(this.backdrop_path);
        dest.writeInt(this.adult ? 1 : 0);
        dest.writeString(this.overview);
        dest.writeString(this.releaseDate);
        dest.writeInt(this.isFavorite ? 1 : 0);
    }

    protected Movie(Parcel in) {
        this.voteCount = in.readInt();
        this.id = in.readInt();
        this.video = in.readInt() == 1;
        this.voteAverage = in.readFloat();
        this.title = in.readString();
        this.popularity = in.readFloat();
        this.posterPath = in.readString();
        this.originalLanguage = in.readString();
        this.originalTitle = in.readString();
        this.genre_ids = in.readArrayList(Integer.class.getClassLoader());
        this.backdrop_path = in.readString();
        this.adult = in.readInt() == 1;
        this.overview = in.readString();
        this.releaseDate = in.readString();
        this.isFavorite = in.readInt() == 1;
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {

        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public boolean equalsId(@Nullable Object obj) {
        if (obj instanceof Movie) {
            return this.id == ((Movie) obj).getId();
        } else {
            return false;
        }
    }
}


