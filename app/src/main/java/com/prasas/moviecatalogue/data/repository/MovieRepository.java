package com.prasas.moviecatalogue.data.repository;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.prasas.moviecatalogue.R;
import com.prasas.moviecatalogue.core.BaseRepository;
import com.prasas.moviecatalogue.dao.MovieDao;
import com.prasas.moviecatalogue.data.entity.Genre;
import com.prasas.moviecatalogue.data.entity.GenreByMovie;
import com.prasas.moviecatalogue.data.entity.Message;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.database.AppDatabase;
import com.prasas.moviecatalogue.utils.OnGetGenreCallback;
import com.prasas.moviecatalogue.utils.OnGetMovieCallback;

import java.util.List;

public class MovieRepository extends BaseRepository {

    private static volatile MovieRepository movieRepository;

    private MutableLiveData<List<Movie>> nowPlayingMovieListLiveData;
    private MutableLiveData<Message> messageNowPlayingMovieList;

    private MutableLiveData<List<Movie>> movieListLiveData;
    private MutableLiveData<Message> messageMovieList;

    private MutableLiveData<List<TvMovie>> tvMovieListLiveData;
    private MutableLiveData<Message> messageTvMovieList;

    private MutableLiveData<List<Movie>> searchMovieListLiveData;
    private MutableLiveData<Message> messageSearchMovieList;

    private MutableLiveData<List<TvMovie>> searchTvMovieListLiveData;
    private MutableLiveData<Message> messageSearchTvMovieList;

    private Application application;

    private MovieDao movieDao;
    private LocalRepository localRepository;
    private RemoteRepository remoteRepository;
    private LiveData<List<Movie>> favoriteMovieList;
    private LiveData<List<TvMovie>> favoriteTvMovieList;

    private LiveData<List<GenreByMovie>> genreMovieList;

    private MovieRepository(Application application) {
        this.application = application;

        AppDatabase appDatabase = AppDatabase.getInstance(application);
        movieDao = appDatabase.getMovieDao();
        localRepository = LocalRepository.getInstance(movieDao, appDatabase);
        remoteRepository = RemoteRepository.getInstance();

        initLiveData();
    }

    private void initLiveData(){
        favoriteMovieList = movieDao.getAllMovie();
        favoriteTvMovieList = movieDao.getAllTvMovie();

        nowPlayingMovieListLiveData = new MutableLiveData<>();
        movieListLiveData = new MutableLiveData<>();
        tvMovieListLiveData = new MutableLiveData<>();
        searchMovieListLiveData = new MutableLiveData<>();
        searchTvMovieListLiveData = new MutableLiveData<>();

        messageNowPlayingMovieList = new MutableLiveData<>();
        messageMovieList = new MutableLiveData<>();
        messageTvMovieList = new MutableLiveData<>();
        messageSearchMovieList = new MutableLiveData<>();
        messageSearchTvMovieList = new MutableLiveData<>();

        genreMovieList = movieDao.getAllGenreByMovie();
    }

    public static MovieRepository getInstance(Application application){
        if (movieRepository == null){
            synchronized (MovieRepository.class){
                if (movieRepository == null){
                    movieRepository = new MovieRepository(application);
                }
            }
        }
        return movieRepository;
    }

    public LiveData<List<Movie>> getNowPlayingMovieListLiveData() {
        return nowPlayingMovieListLiveData;
    }

    public LiveData<List<Movie>> getMovieListLiveData() {
        return movieListLiveData;
    }

    public LiveData<List<TvMovie>> getTvMovieListLiveData() {
        return tvMovieListLiveData;
    }

    public LiveData<List<Movie>> getFavoriteMovieList() {
        return favoriteMovieList;
    }

    public LiveData<List<TvMovie>> getFavoriteTvMovieList() {
        return favoriteTvMovieList;
    }

    public LiveData<List<Movie>> getSearchMovieListLiveData() {
        return searchMovieListLiveData;
    }

    public LiveData<List<TvMovie>> getSearchTvMovieListLiveData() {
        return searchTvMovieListLiveData;
    }

    public LiveData<List<GenreByMovie>> getGenreMovieList() {
        return genreMovieList;
    }

    public LiveData<Message> getMessageNowPlayingMovieList() {
        return messageNowPlayingMovieList;
    }

    public LiveData<Message> getMessageMovieList() {
        return messageMovieList;
    }

    public LiveData<Message> getMessageTvMovieList() {
        return messageTvMovieList;
    }

    public void setupNowPlayingMovieListObservable(Context context) {
        remoteRepository.setupNowPlayingMovieListObservable(context, new OnGetMovieCallback<List<Movie>>() {
            @Override
            public void onSuccess(List<Movie> movieList) {
                nowPlayingMovieListLiveData.setValue(movieList);
                messageNowPlayingMovieList.setValue(new Message("", false));
            }

            @Override
            public void onError(Throwable throwable) {
                messageNowPlayingMovieList.setValue(new Message(
                        application.getString(R.string.failed_load_data,
                                application.getString(R.string.now_playing_movies)),
                        true
                ));
            }
        });
    }

    public void setupMovieListObservable(Context context) {
        remoteRepository.setupMovieListObservable(context, new OnGetMovieCallback<List<Movie>>() {
            @Override
            public void onSuccess(List<Movie> movieList) {
                movieListLiveData.setValue(movieList);
                messageMovieList.setValue(new Message("", false));
            }

            @Override
            public void onError(Throwable throwable) {
                messageMovieList.setValue(new Message(
                        application.getString(R.string.failed_load_data,
                                application.getString(R.string.movies)),
                        true
                ));
            }
        });
    }

    public void setupTvMovieListObservable(Context context) {
        remoteRepository.setupTvMovieListObservable(context, new OnGetMovieCallback<List<TvMovie>>() {
            @Override
            public void onSuccess(List<TvMovie> movieList) {
                tvMovieListLiveData.setValue(movieList);
                messageTvMovieList.setValue(new Message("", false));
            }

            @Override
            public void onError(Throwable throwable) {
                messageTvMovieList.setValue(new Message(
                        application.getString(R.string.failed_load_data,
                                application.getString(R.string.tv_movies)),
                        true
                ));
            }
        });
    }

    public void setupSearchMovieListObservable(Context context){
        remoteRepository.setupSearchMovieListObservable(context, new OnGetMovieCallback<List<Movie>>() {
            @Override
            public void onSuccess(List<Movie> movieList) {
                searchMovieListLiveData.setValue(movieList);
                messageSearchMovieList.setValue(new Message("", false));
            }

            @Override
            public void onError(Throwable throwable) {
                messageSearchMovieList.setValue(new Message(
                        application.getString(R.string.failed_load_data,
                                application.getString(R.string.movies)),
                        true
                ));
            }
        });
    }

    public void setupSearchTvMovieListObservable(Context context){
        remoteRepository.setupSearchTvMovieListObservable(context, new OnGetMovieCallback<List<TvMovie>>() {
            @Override
            public void onSuccess(List<TvMovie> movieList) {
                searchTvMovieListLiveData.setValue(movieList);
                messageSearchTvMovieList.setValue(new Message("", false));
            }

            @Override
            public void onError(Throwable throwable) {
                messageSearchTvMovieList.setValue(new Message(
                        application.getString(R.string.failed_load_data,
                                application.getString(R.string.tv_movies)),
                        true
                ));
            }
        });
    }

    public void fetchMovieListFromRemote(int page) {
        remoteRepository.fetchMovieListFromRemote(page);
    }

    public void fetchTvMovieListFromRemote(int page) {
        remoteRepository.fetchTvMovieListFromRemote(page);
    }

    public void fetchNowPlayingMovieListFromRemote(int page) {
        remoteRepository.fetchNowPlayingMovieListFromRemote(page);
    }

    public void fetchSearchMovieListFromRemote(String query) {
        remoteRepository.fetchSearchMovieListFromRemote(query);
    }

    public void fetchSearchTvMovieListFromRemote(String query) {
        remoteRepository.fetchSearchTvMovieListFromRemote(query);
    }

    public void addFavoriteMovie(Movie movie) {
        localRepository.addFavoriteMovie(movie);
    }

    public void addFavoriteTvMovie(TvMovie tvMovie) {
        localRepository.addFavoriteTvMovie(tvMovie);
    }

    public void removeFavoriteMovie(Movie movie) {
        localRepository.removeFavoriteMovie(movie);
    }

    public void removeFavoriteTvMovie(TvMovie tvMovie) {
        localRepository.removeFavoriteTvMovie(tvMovie);
    }

    public Movie getMovie(int id){
        return localRepository.getMovie(id);
    }

    public void getGenreList(Context context, OnGetGenreCallback callback){
        remoteRepository.getGenreList(context, new OnGetGenreCallback() {
            @Override
            public void onSuccess(List<Genre> genreList) {
                callback.onSuccess(genreList);
            }

            @Override
            public void onError(Throwable throwable) {
                callback.onError(throwable);
            }
        });
    }
}
