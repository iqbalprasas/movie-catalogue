package com.prasas.moviecatalogue.data.repository;

import android.content.Context;

import com.prasas.moviecatalogue.BuildConfig;
import com.prasas.moviecatalogue.data.entity.BaseResponse;
import com.prasas.moviecatalogue.data.entity.Genre;
import com.prasas.moviecatalogue.data.entity.Movie;
import com.prasas.moviecatalogue.data.entity.TvMovie;
import com.prasas.moviecatalogue.services.APIService;
import com.prasas.moviecatalogue.services.ConnectionStatus;
import com.prasas.moviecatalogue.services.DisposableManager;
import com.prasas.moviecatalogue.services.RetrofitClient;
import com.prasas.moviecatalogue.utils.OnGetGenreCallback;
import com.prasas.moviecatalogue.utils.OnGetMovieCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Response;

import static com.prasas.moviecatalogue.utils.Constant.NUM_OF_SLIDE;

public class RemoteRepository {

    private APIService apiService;
    private static RemoteRepository remoteRepository;

    private PublishSubject<Integer> paginationMovieList;
    private PublishSubject<Integer> paginationTvMovieList;
    private PublishSubject<Integer> paginationNowPlayingMovieList;
    private PublishSubject<String> querySearchMovieList;
    private PublishSubject<String> querySearchTvMovieList;
    private PublishSubject<String> dateReleaseMovieList;

    private RemoteRepository() {
        paginationMovieList = PublishSubject.create();
        paginationTvMovieList = PublishSubject.create();
        paginationNowPlayingMovieList = PublishSubject.create();
        querySearchMovieList = PublishSubject.create();
        querySearchTvMovieList = PublishSubject.create();
        dateReleaseMovieList = PublishSubject.create();

        apiService = RetrofitClient.getClient().create(APIService.class);
    }

    public static RemoteRepository getInstance() {
        if (remoteRepository == null) {
            synchronized (RemoteRepository.class) {
                if (remoteRepository == null) {
                    remoteRepository = new RemoteRepository();
                }
            }
        }

        return remoteRepository;
    }

    void setupMovieListObservable(Context context, OnGetMovieCallback<List<Movie>> callback) {
        DisposableManager.getInstance()
                .addDisposable(context, observable(subject(paginationMovieList, context, callback)
                                .flatMap(page -> getMovieList(page).onErrorResumeNext(Observable.empty()))
                        )
                                .doOnError(callback::onError)
                                .subscribe(response -> {
                                    if (response.body() != null && response.isSuccessful()) {
                                        callback.onSuccess(response.body().getResults());
                                    } else {
                                        callback.onError(new Throwable());
                                    }
                                }, callback::onError)
                );
    }

    void setupTvMovieListObservable(Context context, OnGetMovieCallback<List<TvMovie>> callback) {
        DisposableManager.getInstance()
                .addDisposable(context, observable(subject(paginationTvMovieList, context, callback)
                                .flatMap(page -> getTvMovieList(page).onErrorResumeNext(Observable.empty()))
                        )
                                .doOnError(callback::onError)
                                .subscribe(response -> {
                                    if (response.body() != null && response.isSuccessful()) {
                                        callback.onSuccess(response.body().getResults());
                                    } else {
                                        callback.onError(new Throwable());
                                    }
                                }, callback::onError)
                );
    }

    void setupNowPlayingMovieListObservable(Context context, OnGetMovieCallback<List<Movie>> callback) {
        DisposableManager.getInstance()
                .addDisposable(context, observable(subject(paginationNowPlayingMovieList, context, callback)
                                .flatMap(page -> getNowPlayingMovieList(page).onErrorResumeNext(Observable.empty()))
                        )
                                .doOnError(callback::onError)
                                .subscribe(response -> {
                                    List<Movie> tempMovies = new ArrayList<>();
                                    if (response.body() != null && response.isSuccessful()) {
                                        for (int i = 0; i < NUM_OF_SLIDE; i++) {
                                            tempMovies.add(response.body().getResults().get(i));
                                        }
                                        callback.onSuccess(tempMovies);
                                    } else {
                                        callback.onError(new Throwable());
                                    }
                                }, callback::onError)
                );
    }

    void setupSearchMovieListObservable(Context context, OnGetMovieCallback<List<Movie>> callback) {
        DisposableManager.getInstance()
                .addDisposable(context, observable(
                        subject(querySearchMovieList, context, callback)
                                .debounce(300, TimeUnit.MILLISECONDS)
                                .filter(s -> !s.isEmpty())
                                .distinctUntilChanged()
                                .switchMap(query -> getSearchMovieList(query).onErrorResumeNext(Observable.empty()))
                        )
                                .doOnError(callback::onError)
                                .subscribe(response -> {
                                    if (response.body() != null && response.isSuccessful()) {
                                        callback.onSuccess(response.body().getResults());
                                    } else {
                                        callback.onError(new Throwable());
                                    }
                                }, callback::onError)
                );
    }

    void setupSearchTvMovieListObservable(Context context, OnGetMovieCallback<List<TvMovie>> callback) {
        DisposableManager.getInstance()
                .addDisposable(context, observable(
                        subject(querySearchTvMovieList, context, callback)
                                .debounce(300, TimeUnit.MILLISECONDS)
                                .filter(s -> !s.isEmpty())
                                .distinctUntilChanged()
                                .switchMap(query -> getSearchTvMovieList(query).onErrorResumeNext(Observable.empty()))
                        )
                                .doOnError(callback::onError)
                                .subscribe(response -> {
                                    if (response.body() != null && response.isSuccessful()) {
                                        callback.onSuccess(response.body().getResults());
                                    } else {
                                        callback.onError(new Throwable());
                                    }
                                }, callback::onError)
                );
    }

    public void setupReleaseMovieListObservable(Context context, OnGetMovieCallback<List<Movie>> callback) {
        DisposableManager.getInstance()
                .addDisposable(context, dateReleaseMovieList
                        .flatMap(date -> getReleaseTvMovieList(date).onErrorResumeNext(Observable.empty()))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(callback::onError)
                        .subscribe(response -> {
                            if (response.body() != null && response.isSuccessful()) {
                                callback.onSuccess(response.body().getResults());
                            } else {
                                callback.onError(new Throwable());
                            }
                        }, callback::onError)
                );
    }

    void fetchMovieListFromRemote(int page) {
        paginationMovieList.onNext(page);
    }

    void fetchTvMovieListFromRemote(int page) {
        paginationTvMovieList.onNext(page);
    }

    void fetchNowPlayingMovieListFromRemote(int page) {
        paginationNowPlayingMovieList.onNext(page);
    }

    void fetchSearchMovieListFromRemote(String query) {
        querySearchMovieList.onNext(query);
    }

    void fetchSearchTvMovieListFromRemote(String query) {
        querySearchTvMovieList.onNext(query);
    }

    public void fetchReleaseMovieListFromRemote(String date) {
        dateReleaseMovieList.onNext(date);
    }

    private Observable<Response<BaseResponse<ArrayList<Movie>>>> getMovieList(int page) {
        return observable(apiService.getPopularMovies(BuildConfig.API_KEY, page));
    }

    private Observable<Response<BaseResponse<ArrayList<TvMovie>>>> getTvMovieList(int page) {
        return observable(apiService.getPopularTvMovies(BuildConfig.API_KEY, page));
    }

    private Observable<Response<BaseResponse<ArrayList<Movie>>>> getNowPlayingMovieList(int page) {
        return observable(apiService.getNowPlayingMovies(BuildConfig.API_KEY, page));
    }

    private Observable<Response<BaseResponse<ArrayList<Movie>>>> getSearchMovieList(String query) {
        return observable(apiService.getSearchMovies(BuildConfig.API_KEY, query));
    }

    private Observable<Response<BaseResponse<ArrayList<TvMovie>>>> getSearchTvMovieList(String query) {
        return observable(apiService.getSearchTvMovies(BuildConfig.API_KEY, query));
    }

    private Observable<Response<BaseResponse<ArrayList<Movie>>>> getReleaseTvMovieList(String date) {

        return observable(apiService.getReleaseMovies(BuildConfig.API_KEY, date, date));
    }

    void getGenreList(Context context, OnGetGenreCallback callback) {
        observable(apiService.getGenres(BuildConfig.API_KEY)).subscribe(new Observer<Response<ArrayList<Genre>>>() {
            @Override
            public void onSubscribe(Disposable d) {
                DisposableManager.getInstance().addDisposable(context, d);
            }

            @Override
            public void onNext(Response<ArrayList<Genre>> arrayListResponse) {
                callback.onSuccess(arrayListResponse.body());
            }

            @Override
            public void onError(Throwable e) {
                callback.onError(e);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private <T> Observable<T> subject(PublishSubject<T> subject, Context context, OnGetMovieCallback callback) {
        return subject.filter(o -> {
            if (ConnectionStatus.isConnected(context)) {
                return true;
            } else {
                callback.onError(new Throwable());
                return false;
            }
        });
    }

    private <T> Observable<T> observable(Observable<T> observable) {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
